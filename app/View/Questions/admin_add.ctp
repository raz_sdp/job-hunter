<!--<textarea cols="80" id="editor" name="editor1" rows="10">content</textarea>
<textarea cols="80" id="editor1" name="editor1" rows="10">content</textarea>
<textarea cols="80" id="option-1" name="editor1" rows="10">content</textarea>
<textarea cols="80" id="editor1" name="editor1" rows="10">content</textarea>
<textarea cols="80" id="editor1" name="editor1" rows="10">content</textarea>-->

<?php
$default_no_of_ans = 4;
$subject_id = @$this->params['pass'][0];
?>


<div class="wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Question'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="row">
                                <div>
                                    <!--                            --><?php //echo $this->Form->create('Question', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                                    <div class="form-group" style="margin-left: 1px !important">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="col-sm-10">
                                                    <?php echo $this->Form->input('group_id', array(
                                                            'label' => false,
                                                            'class' => 'form-control js-group',
                                                            'options' => ['' =>'Select Group'] + $groups,

                                                        )
                                                    ); ?>
                                                </div>
                                                <!-- /.form-group -->
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="board" class="SubjectBoard" value="0" aria-label="..."   >
                                                    বিষয়ভিত্তিক
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="board" class="SubjectBoard" value="1" aria-label="..."  checked >
                                                    বিশ্ববিদ্যালয়ের বিগত বছরের প্রশ্ন ?
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="board" class="SubjectBoard" value="2" aria-label="..." >
                                                    মডেল টেস্ট
                                                </label>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>


                                    </div>
                                </div>


                            <?php echo $this->Form->create('Question'); ?>
                            <div class="form-group hide">
                                <?php
                                echo $this->Form->input('user_id', ['label' => 'Content Writer','value' => AuthComponent::user()['id'],'class' => 'form-control']);?>
                            </div>
                            
                            <div class="form-group js-subject-div">
                                <?php
                                echo $this->Form->input('subject_id', ['class' => 'form-control select2box', 'value' => @$subject_id]);?>
                            </div>
                            <div class="form-group">
                                <?php //echo $this->Form->input('question', ['id' => 'editor', 'escape' => false]);?>
                                <label>Question</label>
                                <textarea cols="80" id="editor" name="data[Question][question]" rows="10"></textarea>
                            </div>
                            <!--<div class="form-group">
                                <label for="inputEmail3">No of answers</label>
                                <?php /*echo $this->Form->input('no_of_ans', array('label' => false, 'class' => 'form-control', 'options' => $no_of_questions, 'value' => $default_no_of_ans)); */?>
                            </div>-->


                            <div class="form-group">
                                <!--                                <textarea id="option-1" name="data[''][''][]"></textarea>-->
                                <div class="js-ans-input-box">
                                    <?php
                                    for ($count = 1; $count <= $default_no_of_ans; $count++) {
                                        ?>

                                        <div class="form-group">
                                            <label>Choice <?php echo $count ?></label><br>

                                            <div class="col-md-10 row">
                                                <!--                                                --><?php //echo $this->Form->input('Answer.answer.' . $count, array('label' => false, 'type'=> 'textarea','id' => 'option-'.$count, 'escape' => false)); ?>
                                                <textarea id="option-<?php echo $count; ?>" name="data[Answer][answer][]"
                                                          cols="80" rows="10"></textarea>
                                            </div>
                                            <div class="col-md-2">
                                                <!--<input type="checkbox" name="data[Answer][is_correct][<?php /*echo $count */ ?>]" class=""
                                                       id="AnswerIsCorrect">
                                                Accept as Answer-->
                                                <div class="form-group">
                                                    <label>
                                                        <input type="radio" value="<?php echo $count; ?>" id="AnswerIsCorrect"
                                                               name="data[Answer][is_correct]"
                                                               class="flat-red" <?php echo $count == 1 ? 'checked' : '' ?>>
                                                        Accept as Answer
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Answer Description</label>
                                <textarea cols="80" id="editor1" name="data[Question][answer_description]" rows="10"></textarea>
                            </div>
                           <!-- --><?php
/*                            echo $this->Form->input('Test.test', ['class' => 'form-control', 'multiple' => 'multiple']);
                            */?>
                            <br>
                            <div class="col-md-12">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg">Save</button>
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>

<script>
    /*no of question change*/
    $(function () {
        $('#QuestionNoOfAns').on('change', function () {
            var no_of_ans = $(this).val();
            var question_id = $('#test_id').val();
            var data = {
                no_of_ans: no_of_ans,
                question_id: question_id
            };
            $.post(ROOT + 'admin/questions/get_choice_lists', data, function (data) {
                $('.js-ans-input-box').html(data);
            });
        });
    });
    CKEDITOR.replace('editor');
    CKEDITOR.replace('editor1');
    CKEDITOR.replace('option-1');
    CKEDITOR.replace('option-2');
    CKEDITOR.replace('option-3');
    CKEDITOR.replace('option-4');
    CKEDITOR.replace('option-5');
</script>
<script>
    $('.SubjectBoard').on('change', function () {
        var query = $(this).val();
        var group_id = $('.js-group').val();
        commonSearch(query, group_id);
    });
    $('.js-group').on('change', function () {
        var query = $('.SubjectBoard:checked').val();
        var group_id = $(this).val();
        commonSearch(query, group_id);
    });
    function commonSearch(query, group){
        $.get(ROOT + 'admin/subjects/question_add/' + query+'/'+group, function (data) {
            $('.js-subject-div').html(data);
        });
    }
</script>





