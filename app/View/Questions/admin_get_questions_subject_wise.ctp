
<table class="table table-bordered table-striped">
    <thead>
    <th style="width:20px;"><input type="checkbox"></th>
    <th>Select Questions for this Test</th>
    </thead>
    <tbody>
    <?php
    foreach($questions as $key => $question){
        $checked = '';
        $class = '';
        if(in_array($key, $selected_questions)) {
            $checked = 'checked';
            $class = 'success';
        }
        ?>
        <tr class="<?php echo $class?>">
            <td><input type="checkbox" value="<?php echo $key ?>" name="data[TestsQuestion][question_id][]" <?php echo $checked?>></td>
            <td><?php echo $question ?></td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>

<script>
    $('table').tableCheckbox({
        // The class that will be applied to selected rows.
        selectedRowClass: 'success',

        // The selector used to find the checkboxes on the table.
        // You may customize this in order to match your table layout
        //  if it differs from the assumed one.
        checkboxSelector: 'td:first-of-type input[type="checkbox"],th:first-of-type input[type="checkbox"]',

        // A callback that is used to determine wether a checkbox is selected or not.
        isChecked: function ($checkbox) {
            return $checkbox.is(':checked');
        }
    });
</script>