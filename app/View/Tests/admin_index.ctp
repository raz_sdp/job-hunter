<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Test List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <?php echo $this->Form->create('test', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
                            <div class="col-md-5">
                                <div class="form-group">
<!--                                    <label class="col-sm-1 control-label">Group</label>-->
                                    <div class="col-sm-10">
                                        <?php echo $this->Form->input('group_id', array(
                                                'label' => false,
                                                'class' => 'form-control',
                                                'options' => ['' =>'- Select Group -'] + $groups,
                                                'value' => @$group_id
                                            )
                                        ); ?>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <div class="form-group">
<!--                                    <label class="col-sm-1 control-label">Status</label>-->
                                    <div class="col-sm-10">
                                        <?php echo $this->Form->input('status', array(
                                                'label' => false,
                                                'class' => 'form-control',
                                                'options' => ['' =>'Status'] + ['None' => 'None', 'Upcoming' => 'Upcoming','Running '=>'Running', 'Expired' => 'Expired','Done'=> 'Done'],
                                                'value' => @$status
                                            )
                                        ); ?>
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Title"
                                           value="<?php echo $keyword ?>">
                                        <span class="input-group-btn">
                                          <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            </form>
                            <!-- /.col -->
                        </div>





                        <div class="box-body">
                            <div class='table-responsive'>
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('group_id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('title'); ?></th>

                                    <th><?php echo $this->Paginator->sort('test_time'); ?></th>
                                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                                    <th><?php echo $this->Paginator->sort('start_date'); ?></th>
                                    <th><?php echo $this->Paginator->sort('instructions'); ?></th>
                                    <!--<th><?php /*echo $this->Paginator->sort('created'); */?></th>
                                <th><?php /*echo $this->Paginator->sort('modified'); */?></th>-->
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($tests as $test): ?>
                                        <tr>
                                            <td><?php echo h($test['Test']['id']); ?>&nbsp;</td>
                                            <td>
                                                <?php echo $this->Html->link($test['Group']['title'], array('controller' => 'groups', 'action' => 'view', $test['Group']['id'])); ?>
                                            </td>
                                            <td><?php echo h($test['Test']['title']); ?>&nbsp;</td>

                                            <td><?php echo AppHelper::secondsToWords($test['Test']['test_time']); ?>&nbsp;</td>
                                            <td><?php echo h($test['Test']['status']); ?>&nbsp;</td>
                                            <td>
                                                <?php echo date_format(date_create($test['Test']['start_date']), 'd M Y g:i A'); ?>
                                                &nbsp;
                                            </td>
                                            <td><?php echo !empty($test['Test']['instructions']) ? 'Content Available' : 'N/A'; ?>&nbsp;</td>
                                            <!--<td><?php /*echo h($test['Test']['created']); */?>&nbsp;</td>
                                    <td><?php /*echo h($test['Test']['modified']); */?>&nbsp;</td>-->
                                            <td class="actions">
                                                <?php //echo $this->Html->link(__('View'), array('action' => 'view', $test['Test']['id'])); ?>
                                                <?php echo $this->Html->link(__('<span class="glyphicon glyphicon-search" aria-hidden="true"></span> & <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>'), array('action' => 'edit', $test['Test']['id']),array('class'=>'btn btn-primary','escape' => false)); ?>
                                                <?php echo $this->Form->postLink(__('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>'), array('action' => 'delete', $test['Test']['id']),array('class'=>'btn btn-danger','escape' => false), array('confirm' => __('Are you sure you want to delete # %s?', $test['Test']['id']))); ?>
                                                <?php echo $this->Html->link(__('Assign Questions'), array('controller'=>'tests_questions', 'action' => 'assign_ques_into_test', $test['Test']['id'],$test['Group']['id']), ['class'=>'btn btn-info', 'target' => '_blank']); ?>

                                                <?php
                                                if($test['Test']['status']=='Done'){
                                                echo $this->Html->link(__('Result'), array('controller'=>'results', 'action' => 'view', $test['Test']['id']), ['class'=>'btn btn-success', 'target' => '_blank']); ?>
                                                <?php echo $this->Html->link(__('Download'), array('controller'=>'', 'action' => '', $test['Test']['id'],$test['Group']['id']), ['class'=>'btn btn-default']);
                                                }
                                                ?>

                                            </td>
                                        </tr>

                                    <?php endforeach; ?>

                                    </tbody>

                                </table>

                            </div>

                            <p class="pull-right">
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?>
                            </p>
                            <div class="clearfix"></div>
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                                echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                                echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                                ?>
                            </ul>
                        </div>







                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>



