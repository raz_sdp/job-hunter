<div class="wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Edit Model Test'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('Test'); ?>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php echo $this->Form->input('group_id', array('class' => 'form-control js-group')); ?>
                                    </div>

                                    <div class="form-group">
                                        <?php echo $this->Form->input('title', array('class' => 'form-control')); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('test_time', ['class' => 'form-control', 'options' => AppHelper::createTestTimeList()]); ?>
                                    </div>

                                    <div class="form-group">
                                        <?php echo $this->Form->input('status', ['class' => 'form-control', 'options' => ['None' => 'None', 'Upcoming' => 'Upcoming', 'Running ' => 'Running', 'Expired' => 'Expired', 'Done' => 'Done']]); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Exam Datetime</label>
                                        <div class='input-group date' id='start_date'>
                                            <?php
                                            $date = date('m/d/Y H:i A');
                                            if(!empty($this->data['Test']['start_date'])) {
                                                $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $this->data['Test']['start_date']);
                                                $date = $myDateTime->format('m/d/Y H:i A');
                                            }
                                            ?>
                                            <input type='text' class="form-control" name="data[Test][start_date]" value="<?php echo $date?>"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <button class="btn btn-primary btn-block">Update</button>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Instructions</label>
                                        <textarea cols="80" id="instruction" name="data[Test][instructions]"
                                                  rows="10"><?php echo $this->data['Test']['instructions']?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                      </form>


                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>

<script>
    CKEDITOR.replace('instruction');
</script>