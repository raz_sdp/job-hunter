<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <section class="content">
        <div class="row">
            <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Questions List</h3>
                    </div>
                    <?php
                    //pr($questions);die;
                    ?>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <?php echo $this->Form->create('Vendorquestion', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>

                            <!-- /.col -->
                            <div class="col-md-3 col-sm-12">
                                <div class="input-group input-group-sm">
                                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Question name"
                                           value="<?php echo @$keyword ?>">
                                        <span class="input-group-btn">
                                          <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            </form>
                            <!-- /.col -->
                        </div>
                        <?php  //pr($questions);die; ?>
                        <div class='table-responsive'>
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>

<?php   if (AuthComponent::user()['role']=='admin'){   ?><th><?php echo $this->Paginator->sort('company_name','Creator'); ?></th><?php } ?>
                                    <!--			<th>--><?php //echo $this->Paginator->sort('time'); ?><!--</th>-->
                                    <th><?php echo $this->Paginator->sort('Group'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Questions'); ?></th>
                                    <th><?php echo $this->Paginator->sort('answer', 'Correct Answer'); ?></th>
                                    <th><?php echo $this->Paginator->sort('answer_description'); ?></th>
                                    <th><?php echo $this->Paginator->sort('created'); ?></th>
                                    <!--<th><?php echo $this->Paginator->sort('modified'); ?></th>-->
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($questions as $question):
                                    //print_r($question);die;
                                    ?>
                                    <tr>
                                        <td><?php echo h($question['Vendorquestion']['id']); ?>&nbsp;</td>
       <?php   if (AuthComponent::user()['role']=='admin'){   ?><th><?php echo $question['User']['company_name']; ?></th><?php } ?>

                                        <!--		<td>-->
                                       <td> <?php echo h($question['Group']['title']); ?>&nbsp;</td>
                                        <td><?php echo $question['Vendorquestion']['question']; ?>&nbsp;</td>
                                        <td><?php echo @$question['Vendoranswer'][0]['answer']; ?>&nbsp;</td>
                                        <!--		<td>-->
                                        <?php //echo h($question['Question']['point']); ?><!--&nbsp;</td>-->
                                        <!--		<td>-->
                                        <?php //echo h($question['Question']['solution']); ?><!--&nbsp;</td>-->
                                        <td><?php echo h($question['Vendorquestion']['answer_description']); ?>&nbsp;</td>
                                        <td><?php echo h($question['Vendorquestion']['created']); ?>&nbsp;</td>
                                        <!--<td><?php echo h($question['Vendorquestion']['modified']); ?>&nbsp;</td>-->
                                        <td class="actions">
                                            <?php
                                            $vendor_id = '';
                                            if(!empty($this->params['pass']['0'])){
                                            $vendor_id = $this->params['pass']['0'];
                                            }
                                            echo $this->Html->link(__('Edit'), array('action' => 'edit', $question['Vendorquestion']['id'], $vendor_id),['class'=>'btn btn-success']);
                                            ?>
                                            <?php

                                                echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $question['Vendorquestion']['id'], $vendor_id), array('class'=>'btn btn-warning','confirm' => __('Are you sure you want to delete # %s?', $question['Vendorquestion']['id'])));

                                            ?>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>

                                </tbody>

                            </table>
                        </div>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>

                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>' . $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')) . '</li>';
                            echo '<li>' . $this->Paginator->numbers(array('separator' => '')) . '</li>';
                            echo '<li>' . $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')) . '</li>';
                            ?>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>































