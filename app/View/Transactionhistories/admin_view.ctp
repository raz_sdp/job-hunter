<!DOCTYPE html>
<html>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<!-- Left side column. contains the logo and sidebar -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php
    //pr($user)
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Profile
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User profile</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="alert alert-success alert-dismissible js-alert-success1" style="display: none">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success! </strong> This Money has been paid.
        </div>
        <div class="alert alert-danger alert-dismissible js-alert-success2" style="display: none">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Warning! </strong> Something went wrong. Please try again.
        </div>
        <div class="alert alert-danger alert-dismissible js-alert-success3" style="display: none">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Warning! </strong> This request already paid.
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <h1 class="profile-username text-center">Available Balance</h1>
                        <h3 class="text-muted text-center"><?php echo($wallet['Transaction']['available_balance'].' TK')?></h3>

                        <h3 class="profile-username text-center">
                            <?php
                                echo($wallet['User']['fullname']);

                            ?>
                        </h3>
                        <p class="text-muted text-center"><?php echo('Total Withdraw Request <b>'.$totalWithdrowRequest.'</b>')?></p>

                        <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Total Balance</b> <a class="pull-right"><?php echo($wallet['Transaction']['total_point']) ?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Total Withdraw</b> <a class="pull-right"><?php echo($wallet['Transaction']['total_withdraw']) ?></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Contact</b> <a class="pull-right"><?php echo(@$transaction_detail['Transactionhistory']['mobile']) ?></a>
                                </li>
                            <?php
                            $date = $wallet['User']['created'];
                            $d    = explode(' ', $date)
                            ?>


                            <li class="list-group-item">
                                <b>Member Since</b> <a class="pull-right"><?php echo($d[0]) ?></a>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo('TK. '.$transaction_detail['Transactionhistory']['amount']) ?></b> <a class="pull-right"><button class="btn btn-danger button-paid" id="<?php echo $transaction_detail['Transactionhistory']['status']?>"><?php echo $transaction_detail['Transactionhistory']['status'] =='Pending' ? 'paid':'Done' ?></button></a>
                            </li>

                        </ul>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->

                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#settings" data-toggle="tab">Refered Member</a></li>
                    </ul>
                    <div class="tab-content">


                        <div class=" active tab-pane" id="settings">
                            <div class="box-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <tr>
                                        <th><?php echo $this->Paginator->sort('SL.'); ?></th>
                                        <th><?php echo $this->Paginator->sort('Member'); ?></th>
                                        <th><?php echo $this->Paginator->sort('Group'); ?></th>
                                        <th><?php echo $this->Paginator->sort('College'); ?></th>
                                        <th><?php echo $this->Paginator->sort('Joined'); ?></th>
                                    </tr>
                                    <?php $cnt = 1; foreach($total_refer as $refer) {?>
                                    <tr>
                                        <td><?php echo $cnt++?></td>
                                        <td><?php echo $this->Html->link($refer['User']['fullname'], '/users/profile/'.$refer['User']['id']); ?></td>
                                        <td><?php echo $refer['User']['group']?></td>
                                        <td><?php echo $refer['User']['college']?></td>
                                        <?php
                                        $date = $refer['User']['created'];
                                        $d    = explode(' ', $date)
                                        ?>
                                        <td><?php echo $d['0']?></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                                <p class="pull-right">
                                    <?php
                                    echo $this->Paginator->counter(array(
                                        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                    ));
                                    ?>
                                </p>
                                <div class="clearfix"></div>
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <?php
                                    echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                                    echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                                    echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                                    ?>
                                </ul>
                            </div>







                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- Control Sidebar -->
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


</body>
</html>
<script type="application/javascript">


    $('.button-paid').on('click', function () {
        if($('.button-paid').attr('id') == 'Done'){
            $('.js-alert-success3').fadeIn();
        } else {
        var trans_id = <?php echo $transaction_id ?>;
        $.get(ROOT + 'admin/Transactionhistories/paid_money/' + trans_id, function (res) {
            if(res.status){
                $('.js-alert-success1').fadeIn();
                $('.button-paid').text('Done')
            } else {
                $('.js-alert-success2').fadeIn();
            }
        },'json');
        }
    });

</script>