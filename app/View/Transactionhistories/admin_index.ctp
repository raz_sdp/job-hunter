<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Withdraw List</h3>
                    </div>
                    <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Nmae'); ?></th>
                                    <th><?php echo $this->Paginator->sort('Amount'); ?></th>
                                    <th><?php echo $this->Paginator->sort('comment'); ?></th>
                                    <th><?php echo $this->Paginator->sort('status'); ?></th>
                                    <th><?php echo $this->Paginator->sort('created'); ?></th>

                                    <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($transactionhistories as $transactionhistory): ?>
                                    <tr>
                                        <td><?php echo h($transactionhistory['Transactionhistory']['id']); ?>&nbsp;</td>
                                        <td>
                                            <?php echo h($transactionhistory['User']['fullname']); ?>
                                        </td>
                                        <td><?php echo h($transactionhistory['Transactionhistory']['amount']); ?>&nbsp;</td>
                                        <td><?php echo h($transactionhistory['Transactionhistory']['comment']); ?>&nbsp;</td>
                                        <td><?php echo h($transactionhistory['Transactionhistory']['status']); ?>&nbsp;</td>
                                        <td><?php echo h($transactionhistory['Transactionhistory']['created']); ?>&nbsp;</td>
                                        <td class="actions">
                                            <?php echo $this->Html->link(__('Detail'), array('action' => 'view', $transactionhistory['User']['id'],$transactionhistory['Transactionhistory']['id'])); ?>
<!--                                            --><?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $transactionhistory['Transactionhistory']['id'])); ?>
<!--                                            --><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $transactionhistory['Transactionhistory']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $transactionhistory['Transactionhistory']['id']))); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>

                            </table>
                            <p class="pull-right">
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?>
                            </p>
                            <div class="clearfix"></div>
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                                echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                                echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                                ?>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
