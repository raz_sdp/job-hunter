<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'IT Job Hunter : an award wining app ');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>

    <link href='https://fonts.googleapis.com/css?family=Raleway:500,600,700,800,900,400,300' rel='stylesheet' type='text/css'>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
    <?php
    echo $this->Html->meta('favicon.ico','img/favicon.ico',array('type' => 'icon'));

    //echo $this->Html->meta('icon');

    echo $this->Html->css([
        'bootstrap.min.css',
        'owl.carousel.css',
        'Pe-icon-7-stroke.css',
        'font-awesome.min.css',
        'prettyPhoto.css',
        'style.css',
        'animate.css',
        'responsive.css'
    ]);
    echo $this->Html->script([
        'jquery.min.js',
        'bootstrap.min.js',
        'owl.carousel.js',
        'jquery.fitvids.js',
        'smoothscroll.js',
        'jquery.parallax-1.1.3.js',
        'jquery.prettyPhoto.js',
        'jquery.ajaxchimp.min.js',
        'jquery.ajaxchimp.langs.js',
        'wow.min.js',
        'waypoints.min.js',
        'jquery.counterup.min.js',
        'script.js',
        'custom.js',
        'validator.min.js',
    ]);

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>
    <script>
        var ROOT = '<?php echo $this->Html->url('/', true); ?>';
        var HERE = '<?php echo $this->here; ?>';
    </script>
    </head>
    <body>
    <!--Header-->
    <?php echo $this->element('front-header');?>
    <!--/Header-->
    <div id="content">

        <?php echo $this->Session->flash(); ?>

        <?php echo $this->fetch('content'); ?>
    </div>
    <!--Footer-->
    <?php echo $this->element('front-footer'); ?>
    <!--/Footer-->
</body>
</html>
