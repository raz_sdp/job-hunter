<!DOCTYPE html>
<html>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<!-- Left side column. contains the logo and sidebar -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php
    //pr($user)
    ?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        User Profile
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User profile</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

<div class="row">
<div class="col-md-3">

    <!-- Profile Image -->
    <div class="box box-primary">
        <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="<?php echo $this->Html->url('/img/'.$user['User']['pic']) ?>" alt="User profile picture" >

            <h3 class="profile-username text-center">
                <?php
                if($user['User']['role']=='cc'){
                    echo($user['User']['company_name']);
                } else {
                    echo($user['User']['fullname']);
                }
                ?>
            </h3>
<?php
$job = [
    'admin' => 'ADMIN',
    'cw'    => 'Content Writter',
    'cc'    => 'Director',
    'user'  => 'Student'
];
$role = $user['User']['role'];
?>
            <p class="text-muted text-center"><?php echo($job[$role])?></p>

            <ul class="list-group list-group-unbordered">
                <?php if($user['User']['role']=='cc'){ ?>
                <li class="list-group-item">
                    <b>Name</b> <a class="pull-right"><?php echo($user['User']['fullname']) ?></a>
                </li>
                <?php } elseif ($user['User']['role']=='user'){ ?>
                <li class="list-group-item">
                    <b>User Name</b> <a class="pull-right"><?php echo($user['User']['username']) ?></a>
                </li>
                    <li class="list-group-item">
                        <b>Group</b> <a class="pull-right"><?php echo($user['User']['group']) ?></a>
                    </li>
                <?php } ?>

                <li class="list-group-item">
                    <b>City</b> <a class="pull-right"><?php echo($user['User']['city']) ?></a>
                </li>

                <li class="list-group-item">
                    <b>Address</b> <a class="pull-right"><?php echo($user['User']['address']) ?></a>
                </li>
                <li class="list-group-item">
                        <b>Mobile</b> <a class="pull-right"><?php echo($user['User']['mobile']) ?></a>
                </li>
                <li class="list-group-item">
                    <b>Mail</b> <a class="pull-right"><?php echo($user['User']['email']) ?></a>
                </li>
                <?php
                $date = $user['User']['created'];
                $d    = explode(' ', $date)
                ?>


                <li class="list-group-item">
                    <b>Member Since</b> <a class="pull-right"><?php echo($d[0]) ?></a>
                </li>

            </ul>

        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <!-- About Me Box -->

    <!-- /.box -->
</div>
<!-- /.col -->
<div class="col-md-9">
<div class="nav-tabs-custom">
<ul class="nav nav-tabs">
    <li class="active"><a href="#settings" data-toggle="tab">Update Profile</a></li>
</ul>
<div class="tab-content">


<div class=" active tab-pane" id="settings">
    <form class="form-horizontal" method="post">
        <?php if($user['User']['role']=='cc'){ ?>
            <div class="form-group">
                <label for="inputcompany" class="col-sm-2 control-label">Company Name</label>

                <div class="col-sm-10">
                    <input type="text" name="data[User][company_name]" value="<?php echo($user['User']['company_name']) ?>" class="form-control" id="inputcompany" placeholder="Company Name">
                </div>
            </div>
        <?php } elseif ($user['User']['role']=='user'){ ?>
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">User Name</label>

                <div class="col-sm-10">
                    <input type="text" name="data[User][username]" value="<?php echo($user['User']['username']) ?>" class="form-control" id="inputName" placeholder="Name">
                </div>
            </div>
            <?php //pr($group) ?>
            <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Group</label>

                <div class="col-sm-10">
                    <select name="data[User][group]" class="form-control">
                        <?php
                        foreach($group as $key => $title){ ?>
                           <?php $g = $user['User']['group'];?>
                            <option value="<?php echo($title) ?>" <?php echo $g == $title ? 'selected' : ''; ?>><?php echo($title) ?></option>

                      <?php }  ?>
                    </select>

                </div>
            </div>

        <?php } ?>
        <div class="form-group">
            <label for="inputName" class="col-sm-2 control-label">Name</label>

            <div class="col-sm-10">
                <input type="text" name="data[User][fullname]" value="<?php echo($user['User']['fullname']) ?>" class="form-control" id="inputName" placeholder="Name">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="col-sm-2 control-label">Email</label>

            <div class="col-sm-10">
                <input type="email" name="data[User][email]" value="<?php echo($user['User']['email']) ?>" class="form-control" id="inputEmail" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label for="inputaddress" class="col-sm-2 control-label">Address</label>

            <div class="col-sm-10">
                <input type="text" name="data[User][address]" value="<?php echo($user['User']['address']) ?>" class="form-control" id="inputaddress" placeholder="Address">
            </div>
        </div>
        <div class="form-group">
            <label for="inputcity" class="col-sm-2 control-label">City</label>

            <div class="col-sm-10">
                <input class="form-control" name="data[User][city]" value="<?php echo($user['User']['city']) ?>" id="inputcity" placeholder="City">
            </div>
        </div>
        <div class="form-group">
            <label for="inputmobile" class="col-sm-2 control-label">Mobile</label>

            <div class="col-sm-10">
                <input class="form-control" name="data[User][mobile]" value="<?php echo($user['User']['mobile']) ?>" id="inputmobile" placeholder="Mobile Number">
            </div>
        </div>
        <div class="form-group">
            <label for="inputpassword" class="col-sm-2 control-label">Password</label>

            <div class="col-sm-10">
                <input type="text" name="data[User][password]" class="form-control" id="inputpassword" placeholder="Enter New Password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
<!-- /.tab-pane -->
</div>
<!-- /.tab-content -->
</div>
<!-- /.nav-tabs-custom -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->


<!-- Control Sidebar -->
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


</body>
</html>
