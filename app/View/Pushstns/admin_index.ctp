<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Subjects</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- box-Search -->
                        <!-- box-Search -->


                        <div class="box-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('title'); ?></th>
                                    <th><?php echo $this->Paginator->sort('message'); ?></th>
                                    <!--<th><?php /*echo $this->Paginator->sort('created'); */?></th>
                                    <th><?php /*echo $this->Paginator->sort('modified'); */?></th>-->
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($pushstns as $pushstn): ?>
                                    <tr>
                                        <td><?php echo h($pushstn['Pushstn']['id']); ?>&nbsp;</td>
                                        <td><?php echo h($pushstn['Pushstn']['title']); ?>&nbsp;</td>
                                        <td><?php echo h($pushstn['Pushstn']['message']); ?>&nbsp;</td>
                                        <!--<td><?php /*echo h($pushstn['Pushstn']['created']); */?>&nbsp;</td>
                                        <td><?php /*echo h($pushstn['Pushstn']['modified']); */?>&nbsp;</td>-->
                                        <td class="actions">
<!--                                            --><?php //echo $this->Html->link('Send', 'https://console.firebase.google.com/project/admissiontest-dd709/notification/compose', ['target' => '_blank', 'class' => 'btn btn-info']); ?>
                                            <?php echo $this->Html->link(__('Send'), array('action' => 'send_nt', $pushstn['Pushstn']['id']) ,array('confirm' => __('Are you sure you want to send # %s?', $pushstn['Pushstn']['title']),'class' => 'btn btn-warning')); ?>
                                            <?php echo $this->Html->link(__('View'), array('action' => 'view', $pushstn['Pushstn']['id']),['class' => 'btn btn-primary']); ?>
                                            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $pushstn['Pushstn']['id']),['class' => 'btn btn-default']); ?>
                                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $pushstn['Pushstn']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $pushstn['Pushstn']['id']), 'class' => 'btn btn-danger')); ?>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>

                                </tbody>

                            </table>
                            <p class="pull-right">
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?>
                            </p>

                            <div class="clearfix"></div>
                            <ul class="pagination pagination-sm no-margin pull-right">
                                <?php
                                echo '<li>' . $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')) . '</li>';
                                echo '<li>' . $this->Paginator->numbers(array('separator' => '')) . '</li>';
                                echo '<li>' . $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')) . '</li>';
                                ?>
                            </ul>
                        </div>
                        <!-- /.box-body -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
