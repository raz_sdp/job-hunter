<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Admin Add Push</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <?php echo $this->Form->create('Pushstn', array('type' =>'text',"data-toggle" =>"validator", "role" => "form")); ?>
                    <!--                    <form role="form">-->
                    <div class="box-body">
                        <div class="form-group has-feedback">
                            <?php echo $this->Form->input('title', [
                                'class' => 'form-control',
                                'value' => 'IT Job Hunter - ',
                                'type' => 'text',
                                'required' => 'required',
                                'after' => ' <span class="glyphicon form-control-feedback" arial-hidden="true"></span> '

                            ]);
                            ?>
                        </div>

                        <div class="form-group has-feedback">
                            <?php //echo $this->Form->input('question', ['id' => 'editor', 'escape' => false]);?>
                            <label>Message</label>
                            <textarea cols="80" type="text" id="message" name="data[Pushstn][message]" class="form-control" rows="10" required></textarea>
                            <span class="glyphicon form-control-feedback" arial-hidden="true"></span>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<script>
    CKEDITOR.replace('message');
    $("form").submit( function(e) {
        var messageLength = CKEDITOR.instances['message'].getData().replace(/<[^>]*>/gi, '').length;
        if( !messageLength ) {
            alert( 'Please enter a message' );
            e.preventDefault();
        }
    });
</script>