<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Tests Question'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('TestsQuestion',array('type'=>'file')); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <h3><?php echo $this->Form->input('test_id',array('class'=>'form-control'));?></h3>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->input('question_id',array('class'=>'form-control'));?>
                            </div>
                        </div>
                        <?php echo $this->Form->end(__('Submit',['class'=>'Submit'])); ?>
                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>