<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Emails Answer'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('Email'); ?>
                        <div class="box-body">
                            <div class="form-group">

                                <h3><?php echo $this->Form->input('email',array('class'=>'form-control'));?></h3>

                            </div>

                            <div class="form-group">

                                <h3><?php echo $this->Form->input('fullname',array('class'=>'form-control'));?></h3>

                            </div>
                            <div class="form-group">

                                <label>Subject</label>
                                <textarea class="form-control" rows="6" placeholder="" name="data[Email][subject]">  </textarea>

                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea class="form-control" rows="6" placeholder="" name="data[Email][message]" ></textarea>

                            </div>


                        </div>

                        <?php echo $this->Form->end(__('Submit',['class'=>'Submit'])); ?>


                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>






<!--<div class="emails form">
<?php /*echo $this->Form->create('Email'); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Add Email'); */?></legend>
	<?php
/*		echo $this->Form->input('email');
		echo $this->Form->input('fullname');
		echo $this->Form->input('subject');
		echo $this->Form->input('message');
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>

		<li><?php /*echo $this->Html->link(__('List Emails'), array('action' => 'index')); */?></li>
	</ul>
</div>-->
