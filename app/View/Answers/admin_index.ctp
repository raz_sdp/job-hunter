<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Answer Lists</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('id'); ?></th>
                                <th><?php echo $this->Paginator->sort('question_id'); ?></th>
                                <th><?php echo $this->Paginator->sort('answer'); ?></th>
                                <th><?php echo $this->Paginator->sort('feedback'); ?></th>
                                <th><?php echo $this->Paginator->sort('is_correct'); ?></th>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($answers as $answer): ?>
                               <tr>
                                    <td><?php echo h($answer['Answer']['id']); ?>&nbsp;</td>
                                    <td>
                                        <?php echo $this->Html->link($answer['Question']['id'], array('controller' => 'questions', 'action' => 'view', $answer['Question']['id'])); ?>
                                    </td>
                                    <td><?php echo h($answer['Answer']['answer']); ?>&nbsp;</td>
                                    <td><?php echo h($answer['Answer']['feedback']); ?>&nbsp;</td>
                                    <td><?php echo h($answer['Answer']['is_correct']); ?>&nbsp;</td>
                                    <td><?php echo h($answer['Answer']['created']); ?>&nbsp;</td>
                                    <td><?php echo h($answer['Answer']['modified']); ?>&nbsp;</td>
                                    <td class="actions">
                                        <?php echo $this->Html->link(__('View'), array('action' => 'view', $answer['Answer']['id'])); ?>
                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $answer['Answer']['id'])); ?>
                                        <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $answer['Answer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $answer['Answer']['id']))); ?>
                                    </td>
                                </tr>

                            <?php endforeach; ?>

                            </tbody>

                        </table>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>
                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                            echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                            echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                            ?>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>






<!--<div class="answers index">
	<h2><?php /*echo __('Answers'); */?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php /*echo $this->Paginator->sort('id'); */?></th>
			<th><?php /*echo $this->Paginator->sort('question_id'); */?></th>
			<th><?php /*echo $this->Paginator->sort('answer'); */?></th>
			<th><?php /*echo $this->Paginator->sort('feedback'); */?></th>
			<th><?php /*echo $this->Paginator->sort('is_correct'); */?></th>
			<th><?php /*echo $this->Paginator->sort('created'); */?></th>
			<th><?php /*echo $this->Paginator->sort('modified'); */?></th>
			<th class="actions"><?php /*echo __('Actions'); */?></th>
	</tr>
	</thead>
	<tbody>
	<?php /*foreach ($answers as $answer): */?>
	<tr>
		<td><?php /*echo h($answer['Answer']['id']); */?>&nbsp;</td>
		<td>
			<?php /*echo $this->Html->link($answer['Question']['id'], array('controller' => 'questions', 'action' => 'view', $answer['Question']['id'])); */?>
		</td>
		<td><?php /*echo h($answer['Answer']['answer']); */?>&nbsp;</td>
		<td><?php /*echo h($answer['Answer']['feedback']); */?>&nbsp;</td>
		<td><?php /*echo h($answer['Answer']['is_correct']); */?>&nbsp;</td>
		<td><?php /*echo h($answer['Answer']['created']); */?>&nbsp;</td>
		<td><?php /*echo h($answer['Answer']['modified']); */?>&nbsp;</td>
		<td class="actions">
			<?php /*echo $this->Html->link(__('View'), array('action' => 'view', $answer['Answer']['id'])); */?>
			<?php /*echo $this->Html->link(__('Edit'), array('action' => 'edit', $answer['Answer']['id'])); */?>
			<?php /*echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $answer['Answer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $answer['Answer']['id']))); */?>
		</td>
	</tr>
<?php /*endforeach; */?>
	</tbody>
	</table>
	<p>
	<?php
/*	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	*/?>	</p>
	<div class="paging">
	<?php
/*		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	*/?>
	</div>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>
		<li><?php /*echo $this->Html->link(__('New Answer'), array('action' => 'add')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); */?> </li>
	</ul>
</div>
-->