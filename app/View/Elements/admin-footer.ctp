
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2017-<?php echo date("Y");?>  <a href="https://admissionninja.com">IT Job Hunter</a>. </strong>  All rights
    reserved.
  </footer>