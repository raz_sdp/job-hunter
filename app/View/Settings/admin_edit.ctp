
<?php
//pr($this->data);die;
?>

<div class="wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Setting</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <?php echo $this->Form->create('Setting'); ?>
                        <div class="box-body">
                            <div class="row">
                                <?php echo $this->Form->input('id'); ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php echo $this->Form->input('is_free', ['class' => 'checkbox-left']); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>About Page Text</label>
                                        <p class="text-justify">
                                            <?php echo $this->data['Setting']['about_text']?>
                                        </p>
                                    </div>

                                    <div class="form-group">
                                        <label>About Page Text</label>
                                        <textarea cols="80" id="about_text" name="data[Setting][about_text]" value=""
                                                  rows="10"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('version', ['class' => 'form-control']); ?>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn btn-primary btn-block">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>
<script>
    CKEDITOR.replace('about_text');
</script>
