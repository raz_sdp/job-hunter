<div class="vendoranswers form">
<?php echo $this->Form->create('Vendoranswer'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Vendoranswer'); ?></legend>
	<?php
		echo $this->Form->input('vendorquestion_id');
		echo $this->Form->input('answer');
		echo $this->Form->input('is_correct');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Vendoranswers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Vendorquestions'), array('controller' => 'vendorquestions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?> </li>
	</ul>
</div>
