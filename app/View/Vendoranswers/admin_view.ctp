<div class="vendoranswers view">
<h2><?php echo __('Vendoranswer'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vendoranswer['Vendoranswer']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vendorquestion'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vendoranswer['Vendorquestion']['id'], array('controller' => 'vendorquestions', 'action' => 'view', $vendoranswer['Vendorquestion']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Answer'); ?></dt>
		<dd>
			<?php echo h($vendoranswer['Vendoranswer']['answer']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Correct'); ?></dt>
		<dd>
			<?php echo h($vendoranswer['Vendoranswer']['is_correct']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($vendoranswer['Vendoranswer']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($vendoranswer['Vendoranswer']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vendoranswer'), array('action' => 'edit', $vendoranswer['Vendoranswer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vendoranswer'), array('action' => 'delete', $vendoranswer['Vendoranswer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendoranswer['Vendoranswer']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendoranswers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendoranswer'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendorquestions'), array('controller' => 'vendorquestions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?> </li>
	</ul>
</div>
