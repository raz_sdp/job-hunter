<div class="vendoranswers index">
	<h2><?php echo __('Vendoranswers'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('vendorquestion_id'); ?></th>
			<th><?php echo $this->Paginator->sort('answer'); ?></th>
			<th><?php echo $this->Paginator->sort('is_correct'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($vendoranswers as $vendoranswer): ?>
	<tr>
		<td><?php echo h($vendoranswer['Vendoranswer']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($vendoranswer['Vendorquestion']['id'], array('controller' => 'vendorquestions', 'action' => 'view', $vendoranswer['Vendorquestion']['id'])); ?>
		</td>
		<td><?php echo h($vendoranswer['Vendoranswer']['answer']); ?>&nbsp;</td>
		<td><?php echo h($vendoranswer['Vendoranswer']['is_correct']); ?>&nbsp;</td>
		<td><?php echo h($vendoranswer['Vendoranswer']['created']); ?>&nbsp;</td>
		<td><?php echo h($vendoranswer['Vendoranswer']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $vendoranswer['Vendoranswer']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $vendoranswer['Vendoranswer']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $vendoranswer['Vendoranswer']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendoranswer['Vendoranswer']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Vendoranswer'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Vendorquestions'), array('controller' => 'vendorquestions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?> </li>
	</ul>
</div>
