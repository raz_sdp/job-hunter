<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Universities List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
	<table  id="example2" class="table table-bordered table-hover">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($universities as $university): ?>
	<tr>
		<td><?php echo h($university['University']['id']); ?>&nbsp;</td>
		<td><?php echo h($university['University']['title']); ?>&nbsp;</td>
		<td class="actions">
<!--			--><?php //echo $this->Html->link(__('View'), array('action' => 'view', $university['University']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $university['University']['id'])); ?>
<!--			--><?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $university['University']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $university['University']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!--		<li>--><?php //echo $this->Html->link(__('New University'), array('action' => 'add')); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->


            <!-- /.box -->
        </div>
        <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>