<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Results'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('Result'); ?>
                        <div class="box-body">
                            <div class="form-group">

                                <h3><?php echo $this->Form->input('test_id',array('class'=>'form-control'));?></h3>

                            </div>

                            <div class="form-group">

                                <h3><?php echo $this->Form->input('user_id',array('class'=>'form-control'));?></h3>

                            </div>
                            <div class="form-group">

                                <h3><?php echo $this->Form->input('mark',array('class'=>'form-control'));?></h3>


                            </div>



                        </div>

                        <?php echo $this->Form->end(__('Submit',['class'=>'Submit'])); ?>


                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>







<!--<div class="results form">
<?php /*echo $this->Form->create('Result'); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Edit Result'); */?></legend>
	<?php
/*		echo $this->Form->input('id');
		echo $this->Form->input('test_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('mark');
	*/?>
	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>

		<li><?php /*echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Result.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Result.id'))); */?></li>
		<li><?php /*echo $this->Html->link(__('List Results'), array('action' => 'index')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Tests'), array('controller' => 'tests', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Test'), array('controller' => 'tests', 'action' => 'add')); */?> </li>
		<li><?php /*echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); */?> </li>
	</ul>
</div>
-->