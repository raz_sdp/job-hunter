<div class="vendortestsVendorquestions form">
<?php echo $this->Form->create('VendortestsVendorquestion'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Vendortests Vendorquestion'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('vendortest_id');
		echo $this->Form->input('vendorquestion_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('VendortestsVendorquestion.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('VendortestsVendorquestion.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Vendortests Vendorquestions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Vendortests'), array('controller' => 'vendortests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendortest'), array('controller' => 'vendortests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendorquestions'), array('controller' => 'vendorquestions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?> </li>
	</ul>
</div>
