<div class="vendortestsVendorquestions view">
<h2><?php echo __('Vendortests Vendorquestion'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vendortestsVendorquestion['VendortestsVendorquestion']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vendortest'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vendortestsVendorquestion['Vendortest']['title'], array('controller' => 'vendortests', 'action' => 'view', $vendortestsVendorquestion['Vendortest']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vendorquestion'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vendortestsVendorquestion['Vendorquestion']['id'], array('controller' => 'vendorquestions', 'action' => 'view', $vendortestsVendorquestion['Vendorquestion']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($vendortestsVendorquestion['VendortestsVendorquestion']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($vendortestsVendorquestion['VendortestsVendorquestion']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vendortests Vendorquestion'), array('action' => 'edit', $vendortestsVendorquestion['VendortestsVendorquestion']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vendortests Vendorquestion'), array('action' => 'delete', $vendortestsVendorquestion['VendortestsVendorquestion']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendortestsVendorquestion['VendortestsVendorquestion']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendortests Vendorquestions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendortests Vendorquestion'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendortests'), array('controller' => 'vendortests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendortest'), array('controller' => 'vendortests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendorquestions'), array('controller' => 'vendorquestions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?> </li>
	</ul>
</div>
