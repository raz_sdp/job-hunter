<div class="wrapper">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Admin Add Lecture'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->

                        <?php echo $this->Form->create('Lecture',array('type'=>'file')); ?>
                        <div class="box-body">
                            <div class="form-group">

                                <h3><?php echo $this->Form->input('subject_id',array('class'=>'form-control'));?></h3>

                            </div>
                            <div class="form-group">

                                <?php echo $this->Form->input('title',array('class'=>'form-control'));?>

                            </div>
                            <div class="form-group">

                                <label for="exampleInputFullname">Input File</label>
                                <?php echo $this->Form->input('filename',array('type'=>'file','multiple'=>'multiple'));?>
                                <?php echo $this->Html->url('/files/lectures/'.$this->request->data['Lecture']['filename'],true); ?>

                            </div>

                        </div>

                        <?php echo $this->Form->end(__('Submit')); ?>


                    </div>

                </div>

            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>








<!--<div class="lectures form">
<?php /*echo $this->Form->create('Lecture',['type'=>'file']); */?>
	<fieldset>
		<legend><?php /*echo __('Admin Edit Lecture'); */?></legend>
	<?php
/*		echo $this->Form->input('id');
		echo $this->Form->input('subject_id');
		echo $this->Form->input('title');
		echo $this->Form->input('filename',['type'=>'file']);
	*/?>
     <p>

        <?php /*//echo $this->Html->url('/files/PDF/'.)*/?>


     </p>


	</fieldset>
<?php /*echo $this->Form->end(__('Submit')); */?>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>

		<li><?php /*echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Lecture.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Lecture.id'))); */?></li>
		<li><?php /*echo $this->Html->link(__('List Lectures'), array('action' => 'index')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); */?> </li>
	</ul>
</div>
-->