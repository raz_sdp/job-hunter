<div class="content-wrapper" xmlns="http://www.w3.org/1999/html">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                            <h3 class="box-title">Lectures</h3>
</div>
<!-- /.box-header -->

    <div class="box-body">
        <div class="row">
            <?php echo $this->Form->create('Lecture', array('type'=>'get','url' => array('page'=>'1'), 'class' => 'form-horizontal')); ?>
            <div class="col-md-9">
                <div class="form-group">
                    <label class="col-sm-1 control-label">Subject</label>
                    <div class="col-sm-10">
                        <?php echo $this->Form->input('subject_id', array(
                                'label' => false,
                                'class' => 'form-control',
                                'options' => ['' =>'- None -'] + $subjects,
                                'value' => $subject_id
                            )
                        ); ?>
                    </div>
                    <!-- /.form-group -->
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-3">
                <div class="input-group input-group-sm">
                    <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Search by Lecture name"
                           value="<?php echo $keyword ?>">
                                        <span class="input-group-btn">
                                          <button type="submit" class="btn btn-info btn-flat">Go!</button>
                                        </span>
                </div>
                <!-- /.form-group -->
            </div>
            </form>
            <!-- /.col -->
        </div>






<div class="box-body">
<table id="example2" class="table table-bordered table-hover">
<thead>
    <tr>
        <th><?php echo $this->Paginator->sort('id'); ?></th>
        <th><?php echo $this->Paginator->sort('subject_id'); ?></th>
        <th><?php echo $this->Paginator->sort('title'); ?></th>
        <th><?php echo $this->Paginator->sort('filename'); ?></th>
        <th><?php echo $this->Paginator->sort('created'); ?></th>
        <th><?php echo $this->Paginator->sort('modified'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
<tbody>
<?php foreach ($lectures as $lecture): ?>
    <tr>
        <td><?php echo h($lecture['Lecture']['id']); ?>&nbsp;</td>
        <td>
            <?php echo h($lecture['Subject']['title']);?>
        </td>
        <td><?php echo h($lecture['Lecture']['title']); ?>&nbsp;</td>
        <td><?php echo h($lecture['Lecture']['filename']); ?>&nbsp;</td>
        <td><?php echo h($lecture['Lecture']['created']); ?>&nbsp;</td>
        <td><?php echo h($lecture['Lecture']['modified']); ?>&nbsp;</td>
        <td class="actions">
<!--            --><?php //echo $this->Html->link(__('View'), array('action' => 'view', $lecture['Lecture']['id'])); ?>
            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $lecture['Lecture']['id']), array('class' => 'btn btn-success')); ?>
            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $lecture['Lecture']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $lecture['Lecture']['id']), 'class' => 'btn btn-danger')); ?>
        </td>
    </tr>

<?php endforeach; ?>

</tbody>

</table>
    <p class="pull-right">
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>
    </p>
    <div class="clearfix"></div>
    <ul class="pagination pagination-sm no-margin pull-right">
        <?php
        echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
        echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
        echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
        ?>
    </ul>
</div>
<!-- /.box-body -->
</div>
<!-- /.box -->


<!-- /.box -->
</div>
<!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>




<!--
<div class="lectures index">
	<h2><?php /*echo __('Lectures'); */?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php /*echo $this->Paginator->sort('id'); */?></th>
			<th><?php /*echo $this->Paginator->sort('subject_id'); */?></th>
			<th><?php /*echo $this->Paginator->sort('title'); */?></th>
			<th><?php /*echo $this->Paginator->sort('filename'); */?></th>
			<th><?php /*echo $this->Paginator->sort('created'); */?></th>
			<th><?php /*echo $this->Paginator->sort('modified'); */?></th>
			<th class="actions"><?php /*echo __('Actions'); */?></th>
	</tr>
	</thead>
	<tbody>
	<?php /*foreach ($lectures as $lecture): */?>
	<tr>
		<td><?php /*echo h($lecture['Lecture']['id']); */?>&nbsp;</td>
		<td>
			<?php /*echo $this->Html->link($lecture['Subject']['title'], array('controller' => 'subjects', 'action' => 'view', $lecture['Subject']['id'])); */?>
		</td>
		<td><?php /*echo h($lecture['Lecture']['title']); */?>&nbsp;</td>
		<td><?php /*echo h($lecture['Lecture']['filename']); */?>&nbsp;</td>
		<td><?php /*echo h($lecture['Lecture']['created']); */?>&nbsp;</td>
		<td><?php /*echo h($lecture['Lecture']['modified']); */?>&nbsp;</td>
		<td class="actions">
			<?php /*echo $this->Html->link(__('View'), array('action' => 'view', $lecture['Lecture']['id'])); */?>
			<?php /*echo $this->Html->link(__('Edit'), array('action' => 'edit', $lecture['Lecture']['id'])); */?>
			<?php /*echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $lecture['Lecture']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $lecture['Lecture']['id']))); */?>
		</td>
	</tr>
<?php /*endforeach; */?>
	</tbody>
	</table>
	<p>
	<?php
/*	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	*/?>	</p>
	<div class="paging">
	<?php
/*		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	*/?>
	</div>
</div>
<div class="actions">
	<h3><?php /*echo __('Actions'); */?></h3>
	<ul>
		<li><?php /*echo $this->Html->link(__('New Lecture'), array('action' => 'add')); */?></li>
		<li><?php /*echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); */?> </li>
		<li><?php /*echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); */?> </li>
	</ul>
</div>-->
