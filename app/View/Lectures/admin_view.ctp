<div class="lectures view">
<h2><?php echo __('Lecture'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subject'); ?></dt>
		<dd>
			<?php echo $this->Html->link($lecture['Subject']['title'], array('controller' => 'subjects', 'action' => 'view', $lecture['Subject']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Filename'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['filename']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($lecture['Lecture']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Lecture'), array('action' => 'edit', $lecture['Lecture']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Lecture'), array('action' => 'delete', $lecture['Lecture']['id']), array(), __('Are you sure you want to delete # %s?', $lecture['Lecture']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Lectures'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Lecture'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Subjects'), array('controller' => 'subjects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Subject'), array('controller' => 'subjects', 'action' => 'add')); ?> </li>
	</ul>
</div>
