<?php
//pr($vendortests);

?>


<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Test List</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
	<table id="example2" class="table table-bordered table-hover">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
        <?php
        $role = AuthComponent::user(['role']);
        if ($role=='admin'){
        ?>
        <th><?php echo $this->Paginator->sort('Creator'); ?></th>
        <?php } ?>

        <th><?php echo $this->Paginator->sort('group_id'); ?></th>
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('instructions'); ?></th>
			<th><?php echo $this->Paginator->sort('test_time'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('start_date'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($vendortests as $vendortest): ?>
	<tr>
		<td><?php echo h($vendortest['Vendortest']['id']); ?>&nbsp;</td>
        <?php
       $role = AuthComponent::user(['role']);
        if ($role=='admin'){
        ?>
        <td>
            <?php echo h ($vendortest['User']['company_name']); ?>
        </td>
        <?php } ?>
		<td>
			<?php echo h ($vendortest['Group']['title']); ?>
		</td>
		<td><?php echo h($vendortest['Vendortest']['title']); ?>&nbsp;</td>
		<td><?php echo h($vendortest['Vendortest']['instructions']); ?>&nbsp;</td>
		<td><?php echo h($vendortest['Vendortest']['test_time']); ?>&nbsp;</td>
		<td><?php echo h($vendortest['Vendortest']['status']); ?>&nbsp;</td>
		<td><?php echo h($vendortest['Vendortest']['start_date']); ?>&nbsp;</td>
		<td><?php echo h($vendortest['Vendortest']['created']); ?>&nbsp;</td>

		<td class="actions">
			<?php echo $this->Html->link(__('Assign Question'), array( 'controller'=>'VendortestsVendorquestions', 'action' => 'add', $vendortest['Vendortest']['id'], $vendortest['Group']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $vendortest['Vendortest']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $vendortest['Vendortest']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendortest['Vendortest']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
                        <p class="pull-right">
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>
                        </p>
                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
                            <?php
                            echo '<li>'.$this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')).'</li>';
                            echo '<li>'.$this->Paginator->numbers(array('separator' => '')).'</li>';
                            echo '<li>'.$this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')).'</li>';
                            ?>
                        </ul>



                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->


            <!-- /.box -->
        </div>
        <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
