

<div class="wrapper" xmlns="http://www.w3.org/1999/html">
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo __('Add Model Test'); ?></h3>
                        </div>
                        <!-- /.box-header -->
                        <?php //pr(AuthComponent::user()['id'])?>

                        <!-- form start -->
                        <?php echo $this->Form->create('Vendortest'); ?>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php echo $this->Form->input('group_id', array('class' => 'form-control js-group')); ?>
                                    </div>

                                    <div class="form-group">
                                        <?php echo $this->Form->input('title', array('class' => 'form-control')); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php echo $this->Form->input('test_time', ['class' => 'form-control', 'options' => AppHelper::createTestTimeList()]); ?>
                                    </div>

                                    <div class="form-group">
                                        <?php echo $this->Form->input('status', ['class' => 'form-control', 'options' => ['None' => 'None', 'Upcoming' => 'Upcoming', 'Running ' => 'Running', 'Expired' => 'Expired', 'Done' => 'Done']]); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Exam Datetime</label>
                                        <div class='input-group date' id='start_date'>
                                            <input type='text' class="form-control" name="data[Vendortest][start_date]"/>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <button class="btn btn-primary btn-block">Save</button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Instructions</label>
                                        <textarea cols="80" id="instruction" name="data[Vendortest][instructions]"
                                                  rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</div>
<script>
    /*$('.js-group').on('change', function () {
     var group_id = $(this).val();
     $.get(ROOT + 'admin/subjects/get_subject_lists/'+group_id, function (data) {
     $('.js-subject-box').html(data);
     });
     });*/
    CKEDITOR.replace('instruction');
</script>




















<!---->
<!---->
<!---->
<!---->
<!--<div class="vendortests form">-->
<?php //echo $this->Form->create('Vendortest'); ?>
<!--	<fieldset>-->
<!--		<legend>--><?php //echo __('Admin Add Vendortest'); ?><!--</legend>-->
<!--	--><?php
//		echo $this->Form->input('group_id');
//		echo $this->Form->input('title');
//		echo $this->Form->input('instructions');
//		echo $this->Form->input('test_time');
//		echo $this->Form->input('status');
//		echo $this->Form->input('start_date');
//		echo $this->Form->input('user_id');
//		echo $this->Form->input('Vendorquestion');
//	?>
<!--	</fieldset>-->
<?php //echo $this->Form->end(__('Submit')); ?>
<!--</div>-->
<!--<div class="actions">-->
<!--	<h3>--><?php //echo __('Actions'); ?><!--</h3>-->
<!--	<ul>-->
<!---->
<!--		<li>--><?php //echo $this->Html->link(__('List Vendortests'), array('action' => 'index')); ?><!--</li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Vendorresults'), array('controller' => 'vendorresults', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Vendorresult'), array('controller' => 'vendorresults', 'action' => 'add')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('List Vendorquestions'), array('controller' => 'vendorquestions', 'action' => 'index')); ?><!-- </li>-->
<!--		<li>--><?php //echo $this->Html->link(__('New Vendorquestion'), array('controller' => 'vendorquestions', 'action' => 'add')); ?><!-- </li>-->
<!--	</ul>-->
<!--</div>-->
