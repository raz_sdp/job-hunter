<div class="vendorresults form">
<?php echo $this->Form->create('Vendorresult'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Vendorresult'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('vendortest_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('mark');
		echo $this->Form->input('exam_time');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Vendorresult.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Vendorresult.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Vendorresults'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Vendortests'), array('controller' => 'vendortests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendortest'), array('controller' => 'vendortests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
