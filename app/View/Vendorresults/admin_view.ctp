<div class="vendorresults view">
<h2><?php echo __('Vendorresult'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vendorresult['Vendorresult']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vendortest'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vendorresult['Vendortest']['title'], array('controller' => 'vendortests', 'action' => 'view', $vendorresult['Vendortest']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vendorresult['User']['id'], array('controller' => 'users', 'action' => 'view', $vendorresult['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mark'); ?></dt>
		<dd>
			<?php echo h($vendorresult['Vendorresult']['mark']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Exam Time'); ?></dt>
		<dd>
			<?php echo h($vendorresult['Vendorresult']['exam_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($vendorresult['Vendorresult']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($vendorresult['Vendorresult']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vendorresult'), array('action' => 'edit', $vendorresult['Vendorresult']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vendorresult'), array('action' => 'delete', $vendorresult['Vendorresult']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendorresult['Vendorresult']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendorresults'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendorresult'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendortests'), array('controller' => 'vendortests', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendortest'), array('controller' => 'vendortests', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
