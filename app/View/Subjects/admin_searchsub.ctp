<?php
/**
 * Created by PhpStorm.
 * User: mukul
 * Date: 5/22/18
 * Time: 3:51 PM
 */
?>
<select class="form-control js-subject" name="data[TestsQuestion][subject_id]">
    <?php
    foreach($subjects as $key=>$subject){
        $count_sel_ques = AppHelper::countSelectedQuesSubwise($test_id, $key);
        ?>
        <option value="<?php echo $key?>" class="<?php echo !empty($count_sel_ques) ? 'btn-success' : ''?>"><?php echo $subject. ' ('.$count_sel_ques.' questions taken from this subject)'?></option>
    <?php
    }
    ?>
</select>

<script type="application/javascript">
    $('.select2box').select2();

    $('.js-subject').on('change', function () {
        var subject_id = $(this).val();
        var test_id = <?php echo $test_id?>;
        $.get(ROOT + 'admin/questions/get_questions_subject_wise/' + subject_id+'/'+test_id, function (data) {
            $('.js-question-box').html(data);
        });
    });

    $('table').tableCheckbox({
        // The class that will be applied to selected rows.
        selectedRowClass: 'success',

        // The selector used to find the checkboxes on the table.
        // You may customize this in order to match your table layout
        //  if it differs from the assumed one.
        checkboxSelector: 'td:first-of-type input[type="checkbox"],th:first-of-type input[type="checkbox"]',

        // A callback that is used to determine wether a checkbox is selected or not.
        isChecked: function ($checkbox) {
            return $checkbox.is(':checked');
        }
    });
</script>
