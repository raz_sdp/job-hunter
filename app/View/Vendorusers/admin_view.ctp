<div class="vendorusers view">
<h2><?php echo __('Vendoruser'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vendoruser['Vendoruser']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vendor'); ?></dt>
		<dd>
			<?php echo h($vendoruser['Vendoruser']['vendor']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vendoruser['User']['id'], array('controller' => 'users', 'action' => 'view', $vendoruser['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($vendoruser['Vendoruser']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($vendoruser['Vendoruser']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vendoruser'), array('action' => 'edit', $vendoruser['Vendoruser']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vendoruser'), array('action' => 'delete', $vendoruser['Vendoruser']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $vendoruser['Vendoruser']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Vendorusers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vendoruser'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
