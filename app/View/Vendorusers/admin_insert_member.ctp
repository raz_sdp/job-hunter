<?php
/**
 * Created by PhpStorm.
 * User: mukul
 * Date: 5/24/18
 * Time: 3:28 PM
 */
//pr($members);?>
<table class="table table-hover">
    <thead>
    <tr>
        <th><?php echo $this->Paginator->sort('#'); ?></th>
        <th><?php echo $this->Paginator->sort('fullname', 'Full Name'); ?></th>
        <th><?php echo $this->Paginator->sort('Username'); ?></th>
        <!--			<th>--><?php //echo $this->Paginator->sort('time'); ?><!--</th>-->
        <th><?php echo $this->Paginator->sort('Picture'); ?></th>
        <th class="Action">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($members as $key=>$member){ ?>
        <tr>
            <td><?php echo ++$key ?></td>

            <td><?php echo $member['User']['fullname'] ?></td>
            <td><?php echo $member['User']['username'] ?></td>
            <td><?php echo $this->Html->image($member['User']['pic'] , ['height' => '40px','width' => '40px','class' => 'img-circle','alt' => 'User Image'])?>                                            </td>
            <td><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $member['Vendoruser']['id']), array('class'=>'btn btn-danger','confirm' => __('Are you sure you want to kick  %s?', $member['User']['fullname'])));?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<p class="pull-right">
    <?php
    echo $this->Paginator->counter(array(
        'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
    ));
    ?>
</p>

<div class="clearfix"></div>
<ul class="pagination pagination-sm no-margin pull-right">
    <?php
    echo '<li>' . $this->Paginator->prev('< ' . __('Previous'), array(), null, array('class' => 'prev disabled')) . '</li>';
    echo '<li>' . $this->Paginator->numbers(array('separator' => '')) . '</li>';
    echo '<li>' . $this->Paginator->next(__('Next') . ' >', array(), null, array('class' => 'next disabled')) . '</li>';
    ?>
</ul>