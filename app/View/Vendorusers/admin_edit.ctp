<div class="vendorusers form">
<?php echo $this->Form->create('Vendoruser'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Vendoruser'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('vendor');
		echo $this->Form->input('user_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Vendoruser.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Vendoruser.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Vendorusers'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
