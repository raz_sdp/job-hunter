<?php
App::uses('AppModel', 'Model');
/**
 * Vendortest Model
 *
 * @property Group $Group
 * @property User $User
 * @property Vendorresult $Vendorresult
 * @property Vendorquestion $Vendorquestion
 */
class Vendortest extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Vendorresult' => array(
			'className' => 'Vendorresult',
			'foreignKey' => 'vendortest_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Vendorquestion' => array(
			'className' => 'Vendorquestion',
			'joinTable' => 'vendortests_vendorquestions',
			'foreignKey' => 'vendortest_id',
			'associationForeignKey' => 'vendorquestion_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
