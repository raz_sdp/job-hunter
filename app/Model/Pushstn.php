<?php
App::uses('AppModel', 'Model');
/**
 * Pushstn Model
 *
 */
class Pushstn extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

}
