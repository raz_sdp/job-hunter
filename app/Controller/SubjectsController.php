<?php
App::uses('AppController', 'Controller');
/**
 * Subjects Controller
 *
 * @property Subject $Subject
 * @property University $University
 * @property PaginatorComponent $Paginator
 */
class SubjectsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');


    /**
     * @param null $group
     * This was designed for individual. After that we decided we will make it as university wise. look at Universty Controller / get_question_bank (com by Raz)
     */
    /*public function get_question_bank($group = null){
	$this->autoLayout =  false;
	$this->autoRender =  false;
        //ini_set('memory_limit', '-1');
        //ini_set('max_execution_time', 3000000000);
        $this->Subject->Behaviors->load('Containable');
        $query = [
            'fields' => [
                'Subject.title'
            ],

            'contain' => [
                'Question' => ['Answer']
            ],
            'conditions' => [
                'Subject.group_id' => $group,
                //'Subject.group_id' => $group
            ],
	    'limit' => 2,
	    //'order' => 'RAND()',
	    'order' => 'Subject.title ASC',
        ];

        $data = $this->Subject->find('all', $query);
        //pr($data);die;
        $output = [];
        $result = [];
        $res = [];
        foreach($data as $item1){
            $subject = trim($item1['Subject']['title']).' ['.count($item1['Question']).']';
            $output = [];
            $result = [];
            foreach($item1['Question'] as $item){
                $options = [];
                $corr_ans = 'None';
                foreach($item['Answer'] as $key => $answer) {
                    $id =  $key+1;
                    $options = am($options, [
                        "options$id" => strip_tags($answer['answer'],'<sub>,<sup>'),
                    ]);
                    if($answer['is_correct']=='1') {
                        //$corr_ans = strip_tags($answer['answer'],'<sub>,<sup>');
                        $corr_ans = $id;
                    }
                }
                $output[] = am(
                    $result, [
                        'id' => $item['id'],
                        'question' => strip_tags($item['question'],'<sub>,<sup>'),
                    ],
                    $options,
                    ['answer' => $corr_ans]
                );

            }
            $res[] = [$subject => $output];
        }
        if(empty($res)) {
            die(json_encode(['success' => false, 'msg' => 'Question Bank comming soon.']));
        } else {
            die(json_encode(['success' => true, 'question_bank' => $res]));
        }
    }*/

    /**
     * @param null $group
     */
    public function get_lectures($group = null)
    {
        $this->autoRender = false;
        $this->autoLayout = false;
        die(json_encode(['success' => false, 'msg' => 'Lecture\'s comming soon.']));
        $this->Subject->Behaviors->load('Containable');
        $query = [
            'fields' => [
                'Subject.title'
            ],
            'contain' => [
                'Lecture' => [
                    'fields' => ['Lecture.id','Lecture.title','Lecture.filename'],
                    'User' => [
                        'fields' => 'User.id',
                        'UsersLecture' => [
                            'fields' => 'UsersLecture.is_download',
                        ]
                    ]
                ]
            ],
            'conditions' => [
                'Subject.group_id' => $group,
                'Subject.board' => 0
            ]
        ];
        $data = $this->Subject->find('all', $query);
        //print_r($data);die;
        $results = [];
        foreach ($data as $key => $item) {
            $subject = $item['Subject']['title'];
            $lecture_content = array();
            foreach($item['Lecture'] as $lecture){
                $lecture_content[] = [
                    'id' => @$lecture['id'],
                    'title' => @$lecture['title'],
                    'is_download' => !empty($lecture['User'][0]['UsersLecture']['is_download']) ? true : false,
                    'url' => Router::url('/files/lectures/'.$lecture['filename'], true),
                ];
            }
            #$item = Hash::extract($item, 'Lecture.{n}.filename');
            $results[$subject] = $lecture_content;
        }
        //pr($results);die;
        if (empty($results)) {
            die(json_encode(['success' => false, 'msg' => 'Lecture\'s comming soon.']));
        } else {
            die(json_encode(['success' => true, 'lectures' => $results]));
        }
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $keyword = "";
        $conditions = array();
        if(!empty($this->request->data['keyword'])){
            $keyword = $this->request->data['keyword'];
        } elseif(!empty($this->params['named']['keyword'])){
            $keyword = $this->params['named']['keyword'];
        }elseif (!empty($this->params->query['keyword'])) {
            $keyword = $this->params->query['keyword'];
        }
        if (!empty($keyword)){
            $conditions = am($conditions,
                array(
                    'OR'=>
                        array(
                            'Subject.title LIKE' => '%' . $keyword . '%',
                        ),
                )
            );
        }
        $group_id = @$this->params->query['group_id'];
        if(!empty($group_id)){
            $conditions = am($conditions,[
                'Subject.group_id' => $group_id
            ]);
        } else {
            $group_id = '';
        }
        $board = @$this->params->query['board'];
        if(!empty($board)){
            $conditions = am($conditions,[
                'Subject.board' => $board
            ]);
        } else {
            $board = '';
        }

        $university_id = @$this->params->query['university_id'];
        if(!empty($university_id)){
            $conditions = am($conditions,[
                'Subject.university_id' => $university_id
            ]);
        } else {
            $university_id = '';
        }
        $options = array(
            'conditions' => $conditions,
            'order' => 'Subject.id DESC',
        );

        $this->Subject->recursive = 0;
        $this->Paginator->settings = $options;
        $subjects = $this->Paginator->paginate('Subject');
        $groups = $this->Subject->Group->find('list');
        $universities = $this->Subject->University->find('list',['order' => 'University.title ASC']);
        $this->set(compact('subjects','groups','keyword','group_id','universities', 'university_id', 'board'));

    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        if (!$this->Subject->exists($id)) {
            throw new NotFoundException(__('Invalid subject'));
        }
        $options = array('conditions' => array('Subject.' . $this->Subject->primaryKey => $id));
        $this->set('subject', $this->Subject->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            //pr($this->request->data);die;
            $this->Subject->create();
            if ($this->Subject->save($this->request->data)) {
                $this->Session->setFlash(__('The subject has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The subject could not be saved. Please, try again.'));
            }
        }
        $groups = $this->Subject->Group->find('list');
        $universities = $this->Subject->University->find('list',['order' => 'University.title ASC']);
        $this->set(compact('groups','universities'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        if (!$this->Subject->exists($id)) {
            throw new NotFoundException(__('Invalid subject'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Subject->save($this->request->data)) {
                $this->Subject->id = $id;
                $this->Session->setFlash(__('The subject has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The subject could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Subject.' . $this->Subject->primaryKey => $id));
            $this->request->data = $this->Subject->find('first', $options);
        }
        $groups = $this->Subject->Group->find('list');
        $universities = $this->Subject->University->find('list',['order' => 'University.title ASC']);
        $this->set(compact('groups','universities'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        $this->Subject->id = $id;
        if (!$this->Subject->exists()) {
            throw new NotFoundException(__('Invalid subject'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Subject->delete()) {
            $this->Session->setFlash(__('The subject has been deleted.'));
        } else {
            $this->Session->setFlash(__('The subject could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    public function admin_get_subject_lists($group_id = null){
        $this->autoLayout = false;
        $query = [
            'conditions' => [
                'Subject.group_id' => $group_id
            ],
        ];
        $this->Subject->recursive = -1;
        $subjects = $this->Subject->find('list',$query);
        $this->set(compact('subjects'));
    }
    public function admin_searchsub($category = null,$group_id = null, $test_id = null){
        //echo $category;
        $subjects = [];
        $query = [
            'conditions' => [
                'Subject.group_id' => $group_id,
                'Subject.board' => "$category",
            ]
        ];
        $this->Subject->recursive -1;
        $subjects = $this->Subject->find('list', $query);
        $this->set(compact('subjects','test_id'));
    }
    public function admin_subject($category = null,$group_id = null){
        $query = [
            'conditions' => [
                'Subject.group_id' => $group_id,
                'Subject.board' => "$category",
            ]
        ];
        $this->Subject->recursive -1;
        $subjects = $this->Subject->find('list', $query);
        $this->set(compact('subjects'));
    }
    public function admin_question_add($category = null,$group_id = null){
        $query = [
            'conditions' => [
                'Subject.group_id' => $group_id,
                'Subject.board' => "$category",
            ]
        ];
        $this->Subject->recursive -1;
        $subjects = $this->Subject->find('list', $query);
        $this->set(compact('subjects'));
    }
}
