<?php
App::uses('AppController', 'Controller');
/**
 * Results Controller
 *
 * @property Result $Result
 * @property PaginatorComponent $Paginator
 */
class ResultsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
            $conditions = ['Test.status' => 'Done'];
        if(!empty($this->request->data['keyword'])){
            $keyword = $this->request->data['keyword'];
        } elseif(!empty($this->params['named']['keyword'])){
            $keyword = $this->params['named']['keyword'];
        }elseif (!empty($this->params->query['keyword'])) {
            $keyword = $this->params->query['keyword'];
        }
        if (!empty($keyword)){
            $conditions = am($conditions,
                array(
                    'OR'=>
                        array(
                            'Test.title LIKE' => '%' . $keyword . '%',
                        ),
                )
            );
        }
        $group_id = @$this->params->query['group_id'];
        if(!empty($group_id)){
            $conditions = am($conditions,[
                'Test.group_id' => $group_id
            ]);
        } else {
            $group_id = '';
        }
                $options = array(
            'conditions' => $conditions,
            'order' => 'Test.id DESC',
            'fields' => [

                    'Test.id',
                    'Test.title',
                    'Subject.title',
                    'Group.title'

            ]
        );
        $this->Paginator->settings = $options;
        $this->Result->Test->recursive = 0;
        $this->set('tests', $this->Paginator->paginate('Test'));
        $this->loadModel(Group);
        $groups = $this->Group->find('list');
        $this->set(compact('groups', 'group_id','keyword'));


	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($test_id = null) {
        $options = [
            'conditions' =>[
                'Result.test_id' => $test_id
            ],
            'order' => [
                'Result.mark DESC'
            ],
            'fields' => [
                'Result.id',
                'Result.mark',
                'User.username',
                'User.fullname',
                'User.college',
                'Test.title'
            ]
        ];
        $this->Paginator->settings = $options;
        $this->Result->recursive = 0;
        $this->set('results', $this->Paginator->paginate('Result'));

	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Result->create();
			if ($this->Result->save($this->request->data)) {
				$this->Session->setFlash(__('The result has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The result could not be saved. Please, try again.'));
			}
		}
		$tests = $this->Result->Test->find('list');
		$users = $this->Result->User->find('list', ['fields' => ['User.email']]);
		$this->set(compact('tests', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Result->exists($id)) {
			throw new NotFoundException(__('Invalid result'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->Result->id=id;
			if ($this->Result->save($this->request->data)) {
				$this->Session->setFlash(__('The result has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The result could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Result.' . $this->Result->primaryKey => $id));
			$this->request->data = $this->Result->find('first', $options);
		}
		$tests = $this->Result->Test->find('list');
		$users = $this->Result->User->find('list');
		$this->set(compact('tests', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Result->id = $id;
		if (!$this->Result->exists()) {
			throw new NotFoundException(__('Invalid result'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Result->delete()) {
			$this->Session->setFlash(__('The result has been deleted.'));
		} else {
			$this->Session->setFlash(__('The result could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    public function submit_answer(){
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;
        if($this->request->is('post')) {

            /*Test update from upcoming to done */
            /*$this->Result->Test->id = $this->request->data['Result']['test_id'];
            $this->Result->Test->saveField('status', 'done');*/

            /*Result save*/
            $this->Result->create();
            $this->Result->save($this->request->data);
            die(json_encode(array('success' => true, 'msg' => 'Your Answer has been submitted.')));
        } else {
            die(json_encode(array('success' => false, 'msg' => 'Invalid Request.')));
        }
    }
    public function get_result(){
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;
        if($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $test_id = $this->request->data['test_id'];
            $this->Result->Test->recursive = -1;
            $test = $this->Result->Test->findById($test_id);
            $test_title = $test['Test']['title'];
            $my_position_data = $this->Result->getMyPosition($user_id,$test_id);
            $my_mark = $my_position_data[0]['results']['mark'];
            $my_position = $my_position_data[0][0]['rank'];
            $list = $this-> Result-> getTop100($test_id);
            /*echo '<pre>';
            print_r($list);die;*/
            die(json_encode(array('success' => true, 'test_title' => $test_title, 'position' => $my_position, 'mark'=>$my_mark, 'top_100' => $list)));
        } else {
            die(json_encode(array('success' => false, 'msg' => 'Invalid Request.')));
        }
    }

    public function check_users_test_submit_or_not(){
        header('Content-Type: application/json');
        $this->autoLayout = false;
        $this->autoRender = false;
        if($this->request->is('post')) {
            $user_id = $this->request->data['user_id'];
            $test_id = $this->request->data['test_id'];
            $this->Result->recursive = -1;
            $is_exist = $this->Result->findByTestIdAndUserId($test_id, $user_id);
            /*echo '<pre>';
            print_r($is_exist);die;*/
            if(!empty($is_exist)) {
                $test_title = $is_exist['Test']['title'];
                $my_position_data = $this->Result->getMyPosition($user_id,$test_id);
                $my_mark = $my_position_data[0]['results']['mark'];
                $my_position = $my_position_data[0][0]['rank'];

                die(json_encode(array('success' => false, 'msg' => 'You already finished your exam today. Please keep in touch and attend in the next Model Test.','test_title' => $test_title, 'position' => $my_position,'mark'=>$my_mark,)));
            } else {
                die(json_encode(array('success' => true)));
            }
        } else {
            die(json_encode(array('success' => false, 'msg' => 'Invalid Request.')));
        }

    }

}
