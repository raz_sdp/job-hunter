<?php
App::uses('AppController', 'Controller');
/**
 * Vendortests Controller
 *
 * @property Vendortest $Vendortest
 * @property PaginatorComponent $Paginator
 */
class VendortestsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($user = null) {
        if(!empty($user)){
            $user_id = $user;
        } else {
            $user_id = AuthComponent::user()['id'];
        }
        $this->Vendortest->Behaviors->load('containable');
        $query = array(
            'conditions'=> array(
                'Vendortest.user_id'=>$user_id
            ),
            'contain' =>[
                'User' => [
                    'fields' => ['User.company_name', 'User.id'],
                ],
                'Group' => [
                    'fields' => ['Group.id', 'Group.title'],
                ],
            ]
        );
        $this->Paginator->settings = $query;
		$this->set('vendortests', $this->Paginator->paginate('Vendortest'));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Vendortest->exists($id)) {
			throw new NotFoundException(__('Invalid vendortest'));
		}
		$options = array('conditions' => array('Vendortest.' . $this->Vendortest->primaryKey => $id));
		$this->set('vendortest', $this->Vendortest->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
        if ($this->request->is('post')) {

            $this->request->data['Vendortest']['user_id'] = AuthComponent::user()['id'];
            $this->request->data['Vendortest']['instructions'] = preg_replace("/[\n\r]/","",strip_tags($this->request->data['Vendortest']['instructions'], '<sub>,<sup>'));
            if (!empty($this->request->data['Vendortest']['start_date'])) {
                $myDateTime = DateTime::createFromFormat('m/d/Y H:i A', $this->request->data['Vendortest']['start_date']);
                $this->request->data['Vendortest']['start_date'] = $myDateTime->format('Y-m-d H:i:s');
            }
            $this->Vendortest->create();
            if ($this->Vendortest->save($this->request->data)) {

                $this->Session->setFlash(__('The model test has been saved.'));

                return $this->redirect(array('action' => 'index'));

            } else {

                $this->Session->setFlash(__('The model test could not be saved. Please, try again.'));

            }

        }
        $groups = $this->Vendortest->Group->find('list');

        $this->set(compact('groups'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Vendortest->exists($id)) {
			throw new NotFoundException(__('Invalid vendortest'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Vendortest->save($this->request->data)) {
				return $this->flash(__('The vendortest has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('Vendortest.' . $this->Vendortest->primaryKey => $id));
			$this->request->data = $this->Vendortest->find('first', $options);
		}
		$groups = $this->Vendortest->Group->find('list');
		$users = $this->Vendortest->User->find('list');
		$vendorquestions = $this->Vendortest->Vendorquestion->find('list');
		$this->set(compact('groups', 'users', 'vendorquestions'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Vendortest->id = $id;
		if (!$this->Vendortest->exists()) {
			throw new NotFoundException(__('Invalid vendortest'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Vendortest->delete()) {
			return $this->flash(__('The vendortest has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The vendortest could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}

    /**
     * @param null $group_id
     */
    public function admin_get_tests($group_id = null){
        $this->autoLayout = false;
        $user_id = AuthComponent::user()['id'];
        $this->Vendortest->recursive = -1;
        $query = [
            'conditions' =>[
                'Vendortest.group_id'=>$group_id,
                'Vendortest.user_id'=>$user_id
            ]
        ];
        $vendortests = $this->Vendortest->find('list',$query );
        $this->set(compact('vendortests'));
    }
}
