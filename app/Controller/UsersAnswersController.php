<?php
App::uses('AppController', 'Controller');
/**
 * UsersAnswers Controller
 *
 * @property UsersAnswer $UsersAnswer
 * @property PaginatorComponent $Paginator
 */
class UsersAnswersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->UsersAnswer->recursive = 0;
		$this->set('usersAnswers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->UsersAnswer->exists($id)) {
			throw new NotFoundException(__('Invalid users answer'));
		}
		$options = array('conditions' => array('UsersAnswer.' . $this->UsersAnswer->primaryKey => $id));
		$this->set('usersAnswer', $this->UsersAnswer->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->UsersAnswer->create();
			if ($this->UsersAnswer->save($this->request->data)) {
				$this->Session->setFlash(__('The users answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The users answer could not be saved. Please, try again.'));
			}
		}
		$users = $this->UsersAnswer->User->find('list');
		$tests = $this->UsersAnswer->Test->find('list');
		$questions = $this->UsersAnswer->Question->find('list');
		$answers = $this->UsersAnswer->Answer->find('list');
		$this->set(compact('users', 'tests', 'questions', 'answers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->UsersAnswer->exists($id)) {
			throw new NotFoundException(__('Invalid users answer'));
		}
		if ($this->request->is(array('post', 'put'))) {
            $this->UsersAnswer->id=$id;
			if ($this->UsersAnswer->save($this->request->data)) {
				$this->Session->setFlash(__('The users answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The users answer could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UsersAnswer.' . $this->UsersAnswer->primaryKey => $id));
			$this->request->data = $this->UsersAnswer->find('first', $options);
		}
		$users = $this->UsersAnswer->User->find('list');
		$tests = $this->UsersAnswer->Test->find('list');
		$questions = $this->UsersAnswer->Question->find('list');
		$answers = $this->UsersAnswer->Answer->find('list');
		$this->set(compact('users', 'tests', 'questions', 'answers'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->UsersAnswer->id = $id;
		if (!$this->UsersAnswer->exists()) {
			throw new NotFoundException(__('Invalid users answer'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UsersAnswer->delete()) {
			$this->Session->setFlash(__('The users answer has been deleted.'));
		} else {
			$this->Session->setFlash(__('The users answer could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
