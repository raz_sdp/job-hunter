<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    
    public $components = array(
        'Session', 'RequestHandler','Cookie',
        'Auth' => array(
            'loginAction' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
            'loginRedirect' => array('controller' => 'users', 'action' => 'dashboard', 'admin' => true),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
            'authError' => 'You are not allowed',
            'authenticate' => array(
                'Form' => array(
                    'fields' => array('username' => 'email', 'password' => 'password')
                )
            )
        )
    );
    public function beforeFilter(){
        $this->Auth->allow(
            'display','upcoming_test','model_test_questions','model_test_questions', 'submit_answer','get_result','get_lectures','get_question_bank','get_notices','get_all_test','lecture_download_complete',
            'account_update','check_users_test_submit_or_not','point_withdraw','wallet','is_exit_device_id','gad',

            #==========================================old==========================================#
            'login','logout', 'mock_test','held_exam','main','notes','note_edit','note_view',
            'get_tests_categorise','guest','register','save_and_exit', 'register',
            'get_choice_lists', 'create_question_select_box', 'add_rating','my_account','create_selectbox_for_related_question',
            'question_bank','q_bank_practice','ajax_held_exam','ajax_question_bank_practice','users_answer','add',
            'check_my_mock_test_status','paypal_success','paypal_payment','validate_input','paypal_cancel','make_login_false',
            'calculate_limit_offset','update_price','save_devicetoken','save_referpoint'
        );


    }
    /**
     * @param int $max_number
     * @return array
     */
    public function getNumberList($max_number = 0){
        $number_lists = [];
        for($init = 1; $init<=$max_number; $init++) {
            $number_lists[$init] = $init;
        }
        return $number_lists;
    }
    /**
     * upload_file
     *
     */


    public function _upload($file, $folder = null){
        //App::import('Vendor', 'phpthumb', array('file' => 'phpthumb' . DS . 'phpthumb.class.php'));

        if(is_uploaded_file($file['tmp_name'])){

            $ext  = strtolower(array_pop(explode('.',$file['name'])));

            if($ext == 'txt') $ext = 'jpg';

            $fileName = time() . rand(1,999) . '.' .$ext;

            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'pdf' || $ext == 'doc' || $ext == 'docx'){

                $uplodFile = WWW_ROOT.'files'.DS. $folder . DS .$fileName;

                if(move_uploaded_file($file['tmp_name'],$uplodFile)){

                    /*$dest_small = WWW_ROOT . 'files' . DS . $folder . DS . 'small' . DS . $fileName;

                    $this->_resize($uplodFile, $dest_small);*/

                    //$this->_orientation($uplodFile, $uplodFile);

                    //@unlink($uplodFile);

                    return $fileName;

                }

            }

        }

    }

    /**
     * @param array $data
     */
    public function _send_global_push($data = []){
        // Enabling error reporting
        error_reporting(-1);
        ini_set('display_errors', 'On');

        App::import('Vendor', 'firebase', array('file' => 'firebase/firebase.php'));
        App::import('Vendor', 'firebase', array('file' => 'firebase/push.php'));

        $firebase = new Firebase();
       /* $push = new Push();
        $push->setTitle($data['title']);
        $push->setMessage($data['message']);

        $json = $push->getPush();*/
        $response = $firebase->sendToTopic('global', $data);
    }

}
