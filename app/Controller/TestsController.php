<?php

App::uses('AppController', 'Controller');

/**
 * Tests Controller
 *
 * @property Test $Test
 * @property PaginatorComponent $Paginator
 */
class TestsController extends AppController
{


    /**
     * Components
     *
     * @var array
     */

    public $components = array('Paginator');


    public function get_all_test($group_id = null)
    {
        $this->autoLayout = false;
        $this->autoRender = false;
        $this->Test->recursive = -1;
        $query = [
            'fields' => [
                'id', 'Title'
            ],
            'conditions' => [
                'Test.group_id' => $group_id,
                'Test.status' => 'Done',
            ]
        ];
        $data = $this->Test->find('all', $query);
        $result = Hash::extract($data, '{n}.Test');
        if (!empty($result))
            die(json_encode(['success' => true, 'tests' => $result]));
        else
            die(json_encode(['success' => false, 'msg' => 'No Test Finished Yet.']));
    }

    /**
     * admin_index method
     *
     * @return void
     */

    public function admin_index()
    {
        $keyword = "";
        $conditions = array();
        if (!empty($this->request->data['keyword'])) {
            $keyword = $this->request->data['keyword'];
        } elseif (!empty($this->params['named']['keyword'])) {
            $keyword = $this->params['named']['keyword'];
        } elseif (!empty($this->params->query['keyword'])) {
            $keyword = $this->params->query['keyword'];
        }
        if (!empty($keyword)) {
            $conditions = am($conditions,
                array(
                    'OR' =>
                        array(
                            'Test.title LIKE' => '%' . $keyword . '%',
                            'Test.instructions LIKE' => '%' . $keyword . '%',
                            'Test.status LIKE' => '%' . $keyword . '%',
                        ),
                )
            );
        }
        $group_id = @$this->params->query['group_id'];
        if (!empty($group_id)) {
            $conditions = am($conditions, [
                'Test.group_id' => $group_id
            ]);
        } else {
            $group_id = '';
        }

        $status = @$this->params->query['status'];
        if (!empty($status)) {
            $conditions = am($conditions, [
                'Test.status' => $status
            ]);
        } else {
            $status = '';
        }
        $options = array(
            'conditions' => $conditions,
            'order' => 'Test.id DESC',
        );

        $this->Paginator->settings = $options;
        $this->Test->recursive = 0;
        $this->set('tests', $this->Paginator->paginate('Test'));
        $groups = $this->Test->Group->find('list');
        $this->set(compact('groups', 'keyword', 'group_id'));

    }


    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

    public function admin_view($id = null)
    {

        if (!$this->Test->exists($id)) {

            throw new NotFoundException(__('Invalid test'));

        }

        $options = array('conditions' => array('Test.' . $this->Test->primaryKey => $id));

        $this->set('test', $this->Test->find('first', $options));

    }


    /**
     * admin_add method
     *
     * @return void
     */

    public function admin_add()
    {
        if ($this->request->is('post')) {

            //$this->request->data['Test']['instructions'] = preg_replace("/[\n\r]/","",strip_tags($this->request->data['Test']['instructions'], '<sub>,<sup>'));
            if (!empty($this->request->data['Test']['start_date'])) {
                $myDateTime = DateTime::createFromFormat('m/d/Y H:i A', $this->request->data['Test']['start_date']);
                $this->request->data['Test']['start_date'] = $myDateTime->format('Y-m-d H:i:s');
            }
            $this->Test->create();
            if ($this->Test->save($this->request->data)) {

                $this->Session->setFlash(__('The test has been saved.'));

                return $this->redirect(array('action' => 'index'));

            } else {

                $this->Session->setFlash(__('The test could not be saved. Please, try again.'));

            }

        }
        $groups = $this->Test->Group->find('list');

        $this->set(compact('groups'));

    }


    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

    public function admin_edit($id = null)
    {

        if (!$this->Test->exists($id)) {

            throw new NotFoundException(__('Invalid test'));

        }

        if ($this->request->is(array('post', 'put'))) {
            //$this->request->data['Test']['instructions'] = preg_replace("/[\n\r]/","",strip_tags($this->request->data['Test']['instructions'], '<sub>,<sup>'));
            if (!empty($this->request->data['Test']['start_date'])) {
                $myDateTime = DateTime::createFromFormat('m/d/Y H:i A', $this->request->data['Test']['start_date']);
                $this->request->data['Test']['start_date'] = $myDateTime->format('Y-m-d H:i:s');
            }

            $this->Test->id = $id;
            if ($this->Test->save($this->request->data)) {

                $this->Session->setFlash(__('The test has been saved.'));

                return $this->redirect(array('action' => 'index'));

            } else {

                $this->Session->setFlash(__('The test could not be saved. Please, try again.'));

            }

        } else {

            $options = array('recursive' => -1, 'conditions' => array('Test.' . $this->Test->primaryKey => $id));

            $this->request->data = $this->Test->find('first', $options);

        }

        $subjects = $this->Test->Subject->find('list');

        $groups = $this->Test->Group->find('list');

        $questions = $this->Test->Question->find('list', ['fields' => ['Question.question']]);

        $this->set(compact('subjects', 'groups', 'questions'));

    }


    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */

    public function admin_delete($id = null)
    {

        $this->Test->id = $id;

        if (!$this->Test->exists()) {

            throw new NotFoundException(__('Invalid test'));

        }

        $this->request->allowMethod('post', 'delete');

        if ($this->Test->delete()) {

            $this->Session->setFlash(__('The test has been deleted.'));

        } else {

            $this->Session->setFlash(__('The test could not be deleted. Please, try again.'));

        }

        return $this->redirect(array('action' => 'index'));

    }

    public function upcoming_test($group_id = null)
    {
        header('Content-Type: application/json');
        App::uses('CakeTime', 'Utility');
        date_default_timezone_set('Asia/Dhaka');

        $this->autoLayout = false;
        $this->autoRender = false;
        $this->Test->Group->recursive = -1;
        #$group_id = $this->Test->Group->findByTitle($group)['Group']['id'];
        $today = date('Y-m-d H:i:s');


        $test = $this->Test->find('first', [
            'recursive' => -1,
            'fields' => ['Test.id', 'Test.title', 'Test.instructions', 'Test.start_date', 'Test.test_time', 'Test.status'],
            'conditions' => [
                'Test.group_id' => $group_id,
                'Test.status' => 'Upcoming',
            ],
            //'order' => ['Test.start_date' => 'DESC']
            'order' => ['ABS( DATEDIFF( Test.start_date, NOW()))']
        ]);

        //print_r($test);die;
        #$duration = intval($test['Test']['test_time']) / 60;
        #In Sec
        $duration = $test['Test']['test_time'];

        //$d1_exam = date('Y-m-d H:i:s', strtotime($test['Test']['start_date']."+$duration minutes"));
        $dateTime = date_create_from_format('Y-m-d H:i:s', $test['Test']['start_date']);
        #date_add($dateTime, date_interval_create_from_date_string("$duration minutes"));
        date_add($dateTime, date_interval_create_from_date_string("$duration seconds"));

        $d1_exam = date_format($dateTime, 'Y-m-d H:i:s');

        //echo($test['Test']['start_date']);die;
        $d1 = new DateTime($d1_exam);
        $d2 = new DateTime($today);
        /*print_r($d1);
        print_r($d2); die;*/

        if (!empty($test) && $d2 <= $d1) {
            $start_date = new DateTime($today);
            $since_start = $start_date->diff(new DateTime($test['Test']['start_date']));
// print_r($start_date);
 //print_r($since_start);die;
            /*echo $since_start->days.' days total<br>';
            echo $since_start->y.' years<br>';
            echo $since_start->m.' months<br>';
            echo $since_start->d.' days<br>';
            echo $since_start->h.' hours<br>';
            echo $since_start->i.' minutes<br>';
            echo $since_start->s.' seconds<br>';*/

            $is_exam_running = false;
            $exam_time_left = false;
            if ($since_start->y <= 0 && $since_start->m <= 0 && $since_start->d <= 31) {

                /*If exam due time less than 1 day*/
                if ($since_start->d <= 0) {
                    /*Min Calculation*/
                    /*$minutes = $since_start->d * 24 * 60;
                    $minutes += $since_start->h * 60;
                    $minutes += $since_start->i;*/
                    //echo $minutes.' minutes';die;
                    /*End Min calculation*/
                    /*SEC calculation*/
                    $seconds = $since_start->d * 24 * 60*60;
                    $seconds += $since_start->h * 60*60;
                    $seconds += $since_start->i*60;
                    $seconds += $since_start->s;
                    /*End Sec*/
                    #$minutes = $minutes + 1;
                   /* echo $duration.' <br>';
                    echo $seconds.' sec';die;*/
                    if ($since_start->invert && $seconds <= $duration) {
                        $is_exam_running = true;
                        $exam_time_left = $duration - $seconds;
                    }
                    $result = [
                        'id' => $test['Test']['id'],
                        'title' => $test['Test']['title'],
                        'instructions' => $test['Test']['instructions'],
                        /*'left_hour' => $since_start->invert ? 0 : $since_start->h,
                        'left_min' => $since_start->invert ? 0 : $since_start->i,
                        'left_sec' => $since_start->invert ? 0 : $since_start->s,*/
                        'left_sec' => $since_start->invert ? 0 : $seconds,
                        'is_exam_running' => $is_exam_running,
                        #'exam_time_left' => $exam_time_left,
                        #'duration' => $duration,
                        'duration' => $exam_time_left,
                        'exam_date' => CakeTime::format($test['Test']['start_date'], '%B %e, %Y %H:%M %p'),
                    ];
                } else {
                    $result = [
                        'id' => $test['Test']['id'],
                        'title' => $test['Test']['title'],
                        'instructions' => $test['Test']['instructions'],
                        'label_text' => 'Next exam will be held on',
                        /*'duration' => $duration,
                        'exam_time_left' => $exam_time_left,*/
                        'is_exam_running' => $is_exam_running,
                        'duration' => $exam_time_left,
                        'exam_date' => CakeTime::format($test['Test']['start_date'], '%B %e, %Y %H:%M %p'),
                    ];
                }
                die(json_encode(['success' => true, 'upcoming_test' => $result]));
            } else  die(json_encode(['success' => false, 'msg' => 'Exam date isn\'t fixed yet.']));
        } else {
            die(json_encode(array('success' => false, 'msg' => 'No Test Found.')));
        }
    }
}

