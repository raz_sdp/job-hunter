<?php
App::uses('AppController', 'Controller');
/**
 * Universities Controller
 *
 * @property University $University
 * @property PaginatorComponent $Paginator
 */
class UniversitiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->University->recursive = 0;
		$this->set('universities', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->University->exists($id)) {
			throw new NotFoundException(__('Invalid university'));
		}
		$options = array('conditions' => array('University.' . $this->University->primaryKey => $id));
		$this->set('university', $this->University->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->University->create();
			if ($this->University->save($this->request->data)) {
				$this->Flash->success(__('The university has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The university could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->University->exists($id)) {
			throw new NotFoundException(__('Invalid university'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->University->save($this->request->data)) {
				$this->Flash->success(__('The university has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The university could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('University.' . $this->University->primaryKey => $id));
			$this->request->data = $this->University->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->University->id = $id;
		if (!$this->University->exists()) {
			throw new NotFoundException(__('Invalid university'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->University->delete()) {
			$this->Flash->success(__('The university has been deleted.'));
		} else {
			$this->Flash->error(__('The university could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    /**
     * @param null $group
     */
    public function get_question_bank($group = null){
        $this->autoLayout =  false;
        $this->autoRender =  false;
        //ini_set('memory_limit', '-1');
        //ini_set('max_execution_time', 3000000000);
        $this->University->Behaviors->load('Containable');
        $query = [
            'contain' => [
                'Subject'=>[
                    'fields' => [
                        'Subject.title'
                    ],
                    'conditions' => [
                        'Subject.group_id' => $group,
                    ],
                    'Question' => ['Answer']
                ],
            ],

            //'limit' => 5,
            //'order' => 'RAND()',
            'order' => 'University.title ASC',
        ];

        $data = $this->University->find('all', $query);
        //pr($data);die;
        $final_output = [];
        foreach($data as $item1){
            $univ_title = !empty($item1['University']['title']) ? $item1['University']['title'] : $item1['Subject']['title'];
            $res = [];
            if(!empty($item1['Subject'])) {
                foreach($item1['Subject'] as $item_sub){
                    $subject = trim($item_sub['title']).' ['.count($item_sub['Question']).']';
                    $output = [];
                    $result = [];
                    foreach($item_sub['Question'] as $item){
                        $options = [];
                        $corr_ans = 'None';
                        foreach($item['Answer'] as $key => $answer) {
                            $id =  $key+1;
                            $options = am($options, [
                                "options$id" => strip_tags($answer['answer'],'<sub>,<sup>'),
                            ]);
                            if($answer['is_correct']=='1') {
                                //$corr_ans = strip_tags($answer['answer'],'<sub>,<sup>');
                                $corr_ans = $id;
                            }
                        }
                        $output[] = am(
                            $result, [
                                'id' => $item['id'],
                                'question' => strip_tags($item['question'],'<sub>,<sup>'),
                            ],
                            $options,
                            ['answer' => $corr_ans]
                        );

                    }
                    $res[] = [$subject => $output];
                }
                $final_output[] = [$univ_title => $res];
            }
        }
        //pr($final_output);die;
        if(empty($final_output)) {
            die(json_encode(['success' => false, 'msg' => 'Question Bank comming soon.']));
        } else {
            die(json_encode(['success' => true, 'question_bank' => $final_output]));
        }
    }

    /*Test whether any bug in question Bank*/
    public function admin_question_bank_bug_search($group = null){
        $this->autoLayout =  false;
        $this->autoRender =  false;
        //ini_set('memory_limit', '-1');
        //ini_set('max_execution_time', 3000000000);
        $this->University->Behaviors->load('Containable');
        $query = [
            'contain' => [
                'Subject'=>[
                    'fields' => [
                        'Subject.title'
                    ],
                    'conditions' => [
                        'Subject.group_id' => $group,
                    ],
                    'Question' => ['Answer']
                ],
            ],

            //'limit' => 5,
            //'order' => 'RAND()',
            'order' => 'University.title ASC',
        ];

        $data = $this->University->find('all', $query);
        //pr($data);die;
        $final_output = [];
        foreach($data as $item1){
            $univ_title = !empty($item1['University']['title']) ? $item1['University']['title'] : $item1['Subject']['title'];

            if(!empty($item1['Subject'])) {
                foreach($item1['Subject'] as $item_sub){
                    $subject_id = $item_sub['id'];
                    $subject = $item_sub['title'];
                    $output = [];
                    $result = [];
                    $res = [];
                    foreach($item_sub['Question'] as $item){
                        //pr($item);die;
                        if(AuthComponent::user()['role']=='cw'){
                            if($item['user_id'] == AuthComponent::user()['id']) {
                                $options = [];
                                $corr_ans = 'None';
                                $count = 0;
                                foreach($item['Answer'] as $key => $answer) {
                                    $id =  $key+1;
                                    $ans = strip_tags($answer['answer'],'<sub>,<sup>');
                                    //echo empty($ans)? 'Raz': "$ans".'<br>';
                                    $options = am($options, [
                                        "options$id" => $ans,
                                    ]);
                                    if($answer['is_correct']=='1') {
                                        //$corr_ans = strip_tags($answer['answer'],'<sub>,<sup>');
                                        $corr_ans = $id;
                                    }
                                    if(!empty($ans))
                                        $count++;
                                }
                                //echo"$count<br>";
                                if($count<4) {
                                    echo "$count - option found in Question= $item[id] Subject Id=".$subject_id . ' And Title is : '. $subject."<br>";
                                }
                                if($corr_ans == 'None') {
                                    echo "No correct Ans in Question $item[id] Subject Id=".$subject_id . ' '. $subject."<br>";
                                }
                            }
                        } else {
                            $options = [];
                            $corr_ans = 'None';
                            $count = 0;
                            foreach($item['Answer'] as $key => $answer) {
                                $id =  $key+1;
                                $ans = strip_tags($answer['answer'],'<sub>,<sup>');
                                //echo empty($ans)? 'Raz': "$ans".'<br>';
                                $options = am($options, [
                                    "options$id" => $ans,
                                ]);
                                if($answer['is_correct']=='1') {
                                    //$corr_ans = strip_tags($answer['answer'],'<sub>,<sup>');
                                    $corr_ans = $id;
                                }
                                if(!empty($ans))
                                    $count++;
                            }
                            //echo"$count<br>";
                            if($count<4) {
                                echo "$count - option found in Question $item[id] Subject Id=".$subject_id . ' '. $subject."<br>";
                            }
                            if($corr_ans == 'None') {
                                echo "No correct Ans in Question $item[id] Subject Id=".$subject_id . ' '. $subject."<br>";
                            }
                        }

                    }

                }
            }
        }
        Exit;

    }
}
