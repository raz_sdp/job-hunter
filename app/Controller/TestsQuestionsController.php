<?php

App::uses('AppController', 'Controller');

/**

 * TestsQuestions Controller

 *

 * @property TestsQuestion $TestsQuestion

 * @property PaginatorComponent $Paginator

 */

class TestsQuestionsController extends AppController {



/**

 * Components

 *

 * @var array

 */

	public $components = array('Paginator');



/**

 * admin_index method

 *

 * @return void

 */

	public function admin_index() {
        $tests = $this->TestsQuestion->Test->find('list');
        $data = $this->model_test_questions(key($tests), false);

        $test_title = current($tests);

        $this->set(compact('tests','data', 'test_title'));
        //$this->render('/TestsQuestions/admin_index');
	}



/**

 * admin_view method

 *

 * @throws NotFoundException

 * @param string $id

 * @return void

 */

	public function admin_view($id = null) {

		if (!$this->TestsQuestion->exists($id)) {

			throw new NotFoundException(__('Invalid tests question'));

		}

		$options = array('conditions' => array('TestsQuestion.' . $this->TestsQuestion->primaryKey => $id));

		$this->set('testsQuestion', $this->TestsQuestion->find('first', $options));

	}



/**

 * admin_add method

 *

 * @return void

 */

	public function admin_add() {

		if ($this->request->is('post')) {

			$this->TestsQuestion->create();

			if ($this->TestsQuestion->save($this->request->data)) {

				$this->Session->setFlash(__('The tests question has been saved.'));

				return $this->redirect(array('action' => 'index'));

			} else {

				$this->Session->setFlash(__('The tests question could not be saved. Please, try again.'));

			}

		}

		$tests = $this->TestsQuestion->Test->find('list');

		$questions = $this->TestsQuestion->Question->find('list');

		$this->set(compact('tests', 'questions'));

	}



/**

 * admin_edit method

 *

 * @throws NotFoundException

 * @param string $id

 * @return void

 */

	public function admin_edit($id = null) {

		if (!$this->TestsQuestion->exists($id)) {

			throw new NotFoundException(__('Invalid tests question'));

		}

		if ($this->request->is(array('post', 'put'))) {

            $this->TestsQuestion->id= $id;
			if ($this->TestsQuestion->save($this->request->data)) {

				$this->Session->setFlash(__('The tests question has been saved.'));

				return $this->redirect(array('action' => 'index'));

			} else {

				$this->Session->setFlash(__('The tests question could not be saved. Please, try again.'));

			}

		} else {

			$options = array('conditions' => array('TestsQuestion.' . $this->TestsQuestion->primaryKey => $id));

			$this->request->data = $this->TestsQuestion->find('first', $options);

		}

		$tests = $this->TestsQuestion->Test->find('list');

		$questions = $this->TestsQuestion->Question->find('list');

		$this->set(compact('tests', 'questions'));

	}



/**

 * admin_delete method

 *

 * @throws NotFoundException

 * @param string $id

 * @return void

 */

	public function admin_delete($id = null) {

		$this->TestsQuestion->id = $id;

		if (!$this->TestsQuestion->exists()) {

			throw new NotFoundException(__('Invalid tests question'));

		}

		$this->request->allowMethod('post', 'delete');

		if ($this->TestsQuestion->delete()) {

			$this->Session->setFlash(__('The tests question has been deleted.'));

		} else {

			$this->Session->setFlash(__('The tests question could not be deleted. Please, try again.'));

		}

		return $this->redirect(array('action' => 'index'));

	}

  public function model_test_questions($test_id = null, $api = true){
        header('Content-Type: application/json');
        /*$this->autoLayout = false;
        $this->autoRender = false;*/
//        $conditions = array();
//        $conditions = am($conditions, ['TestsQuestion.test_id' => $test_id]);

        $selected_test_questions_data = $this->TestsQuestion->find('all', [
            'fields' => ['TestsQuestion.question_id'],
            'conditions' => ['TestsQuestion.test_id' => $test_id],
        ]);
        $selected_test_questions = Hash::extract($selected_test_questions_data,'{n}.TestsQuestion.question_id');

        //AuthComponent::_setTrace($subject_ids);
        $testsQuestions = [];
        if(!empty($selected_test_questions)) {
            $subject_cond = [];
            if(count($selected_test_questions)>1){
                $subject_cond = ['Question.id IN' => $selected_test_questions];
            } else {
                $subject_cond = ['Question.id' => $selected_test_questions[0]];
            }
            $query = array(
                'contain' => [
                    'Answer' => [
                        'fields' => ['Answer.id','Answer.answer','Answer.is_correct']
                    ],
                ],
                //'recursive' => -1,
                'fields' => array('Question.id','Question.question'),
                'conditions' => $subject_cond
            );
            $this->TestsQuestion->Question->Behaviors->load('Containable');
            $testsQuestions =  $this->TestsQuestion->Question->find('all', $query);
            /*Make it old format*/
            $output = [];
            $result = [];
            foreach($testsQuestions as $item){
                $options = [];
                $corr_ans = 'None';
                foreach($item['Answer'] as $key => $answer) {
                    $id =  $key+1;
                    $options = am($options, [
                        "options$id" => strip_tags($answer['answer'],'<sub>,<sup>'),
                    ]);
                    if($answer['is_correct']=='1') {
                        //$corr_ans = strip_tags($answer['answer'],'<sub>,<sup>');
                        $corr_ans = $id;
                    }
                }
                $output[] = am(
                    $result, [
                        'id' => $item['Question']['id'],
                        'question' => strip_tags($item['Question']['question'],'<sub>,<sup>'),
                    ],
                    $options,
                    ['answer' => $corr_ans]
                );
            }
            /*End Old format*/
            if($api)
            die(json_encode($output));
            else return $output;
        } /*else {
            die(json_encode(['success'=> false, 'msg' => 'No Questions Found']));
        }*/
    }

    public function admin_assign_ques_into_test($test_id = null, $group_id = null){
        if ($this->request->is('post')) {
            //pr($this->request->data);die;
            $tests_questions = $this->request->data['TestsQuestion']['question_id'];
            $subject_id = $this->request->data['TestsQuestion']['subject_id'];

            // Delete old questions
            $this->TestsQuestion->deleteAll(['TestsQuestion.test_id' => $test_id, 'TestsQuestion.subject_id' => $subject_id]);

            if(!empty($tests_questions)) {
                foreach($tests_questions as $item){
                    $data['TestsQuestion']= array(
                        'test_id' => $test_id,
                        'subject_id' => $subject_id,
                        'question_id' => $item
                    );
                    $this->TestsQuestion->create();
                    $this->TestsQuestion->save($data);
                }
            }

            $this->Session->setFlash(__('The questions has been assigned into this Test.'),'default',['class' => 'alert alert-success text-center']);
            return $this->redirect(array('action' => 'assign_ques_into_test', $test_id,$group_id));
        }else{
            $this->loadModel('Subject');
            $this->loadModel('Group');
            $groups = $this->Group->find('list');
            $query = [
                'conditions' => [
                    'Subject.group_id' => $group_id,
                    'Subject.board'    => '1'
                ]
            ];
            $subjects = $this->Subject->find('list',$query);


            $query_1 = [
                'fields' => ['Question.question'],
                'conditions' => [
                    'Question.subject_id' => key($subjects)
                ],
            ];

            $options = array('fields'=> array('TestsQuestion.question_id'),'conditions' => array('TestsQuestion.test_id'  => $test_id));
            $selected_questions = $this->TestsQuestion->find('list', $options);

            $query_2 = [
                'recursive' => -1,
                 'conditions' => [
                      'Test.id' => $test_id
                 ],
                'fields' => [
                    'Test.id','Test.title'
                ]
            ];
            $tests = $this->TestsQuestion->Test->find('first', $query_2);
            $questions = $this->TestsQuestion->Question->find('list', $query_1);
            $this->set(compact('test_id','tests','subjects', 'questions','selected_questions','groups','group_id'));
        }
    }
    public function admin_test_view(){
        $this->autoLayout = false;
        if($this->request->is('post')) {
            $test_id = $this->request->data['test_id'];
            $test_title = $this->request->data['test_title'];

            $data = $this->model_test_questions($test_id, false);
            $this->set(compact('data','test_title'));
            //$this->render('/TestsQuestions/admin_test_view');
        }
    }
}

