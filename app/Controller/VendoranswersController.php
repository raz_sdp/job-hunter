<?php
App::uses('AppController', 'Controller');
/**
 * Vendoranswers Controller
 *
 * @property Vendoranswer $Vendoranswer
 * @property PaginatorComponent $Paginator
 */
class VendoranswersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Vendoranswer->recursive = 0;
		$this->set('vendoranswers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Vendoranswer->exists($id)) {
			throw new NotFoundException(__('Invalid vendoranswer'));
		}
		$options = array('conditions' => array('Vendoranswer.' . $this->Vendoranswer->primaryKey => $id));
		$this->set('vendoranswer', $this->Vendoranswer->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Vendoranswer->create();
			if ($this->Vendoranswer->save($this->request->data)) {
				return $this->flash(__('The vendoranswer has been saved.'), array('action' => 'index'));
			}
		}
		$vendorquestions = $this->Vendoranswer->Vendorquestion->find('list');
		$this->set(compact('vendorquestions'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Vendoranswer->exists($id)) {
			throw new NotFoundException(__('Invalid vendoranswer'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Vendoranswer->save($this->request->data)) {
				return $this->flash(__('The vendoranswer has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('Vendoranswer.' . $this->Vendoranswer->primaryKey => $id));
			$this->request->data = $this->Vendoranswer->find('first', $options);
		}
		$vendorquestions = $this->Vendoranswer->Vendorquestion->find('list');
		$this->set(compact('vendorquestions'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Vendoranswer->id = $id;
		if (!$this->Vendoranswer->exists()) {
			throw new NotFoundException(__('Invalid vendoranswer'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Vendoranswer->delete()) {
			return $this->flash(__('The vendoranswer has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The vendoranswer could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
}
