<?php
App::uses('AppController', 'Controller');
/**
 * VendortestsVendorquestions Controller
 *
 * @property VendortestsVendorquestion $VendortestsVendorquestion
 * @property PaginatorComponent $Paginator
 */
class VendortestsVendorquestionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->VendortestsVendorquestion->recursive = 0;
		$this->set('vendortestsVendorquestions', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->VendortestsVendorquestion->exists($id)) {
			throw new NotFoundException(__('Invalid vendortests vendorquestion'));
		}
		$options = array('conditions' => array('VendortestsVendorquestion.' . $this->VendortestsVendorquestion->primaryKey => $id));
		$this->set('vendortestsVendorquestion', $this->VendortestsVendorquestion->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add($test_id = null, $group_id = null) {


        if ($this->request->is('post')) {
            $tests_questions = $this->request->data['VendortestsVendorquestion']['vendorquestion_id'];
            $test_id = $this->request->data['VendortestsVendorquestion']['vendortest_id'];

            // Delete old questions
            $this->VendortestsVendorquestion->deleteAll(['VendortestsVendorquestion.vendortest_id' => $test_id]);

            if(!empty($tests_questions)) {
                foreach($tests_questions as $item){
                    $data['VendortestsVendorquestion']= array(
                        'vendortest_id' => $test_id,
                        'vendorquestion_id' => $item
                    );
                    $this->VendortestsVendorquestion->create();
                    $this->VendortestsVendorquestion->save($data);
                }
            }

            $this->Session->setFlash(__('The questions has been assigned into this Model Test.'),'default',['class' => 'alert alert-success text-center']);
            return $this->redirect(array('controller' => 'Vendortests' , 'action' => 'admin_index'));
        }else{
            $user_id = AuthComponent::user()['id'];
            $this->VendortestsVendorquestion->Vendorquestion->recursive = -1;
            $query = [
                'fields' => ['Vendorquestion.question'],
                'conditions' => [
                    'Vendorquestion.group_id' => $group_id,
                    'Vendorquestion.user_id'  => $user_id
                ],
            ];
            $questions1 = $this->VendortestsVendorquestion->Vendorquestion->find('list',$query );

            $options = array('fields'=> array('VendortestsVendorquestion.vendorquestion_id'),'conditions' => array('VendortestsVendorquestion.vendortest_id'  => $test_id));
            $selected_questions = $this->VendortestsVendorquestion->find('list', $options);
            $this->VendortestsVendorquestion->Vendortest->recursive = -1;
            $query1 = [
                'fields' => ['Vendortest.id', 'Vendortest.title'],
                'conditions' => [
                    'Vendortest.group_id' => $group_id,
                    'Vendortest.user_id'  => $user_id,
                    'Vendortest.id'  => $test_id
                ]
            ];
            $tests = $this->VendortestsVendorquestion->Vendortest->find('first',$query1 );

            $this->set(compact('tests', 'questions1','groups','selected_questions', 'TestGroup'));
        }

	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->VendortestsVendorquestion->exists($id)) {
			throw new NotFoundException(__('Invalid vendortests vendorquestion'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->VendortestsVendorquestion->save($this->request->data)) {
				return $this->flash(__('The vendortests vendorquestion has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('VendortestsVendorquestion.' . $this->VendortestsVendorquestion->primaryKey => $id));
			$this->request->data = $this->VendortestsVendorquestion->find('first', $options);
		}
		$vendortests = $this->VendortestsVendorquestion->Vendortest->find('list');
		$vendorquestions = $this->VendortestsVendorquestion->Vendorquestion->find('list');
		$this->set(compact('vendortests', 'vendorquestions'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->VendortestsVendorquestion->id = $id;
		if (!$this->VendortestsVendorquestion->exists()) {
			throw new NotFoundException(__('Invalid vendortests vendorquestion'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->VendortestsVendorquestion->delete()) {
			return $this->flash(__('The vendortests vendorquestion has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The vendortests vendorquestion could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
}
