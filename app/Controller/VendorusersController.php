<?php
App::uses('AppController', 'Controller');
/**
 * Vendorusers Controller
 *
 * @property Vendoruser $Vendoruser
 * @property PaginatorComponent $Paginator
 */
class VendorusersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {


            $this->Vendoruser->recursive = 0;
            $this->set('vendorusers', $this->Paginator->paginate());

    }
    public function admin_insert_member($user_id = null, $vendor_id = null){
        if ($this->Vendoruser->find('first', ['conditions' => ['Vendoruser.user_id' => $user_id]] )) {
            die(json_encode(array('success' => false, 'msg' => 'Sorry! User already exist.')));
        } else {
        if(!empty($vendor_id)){
            $vendor = $vendor_id;
        } else {
            $vendor = AuthComponent::user()['id'];
        }
        $this->request->data['Vendoruser']['user_id'] = $user_id;
        $this->request->data['Vendoruser']['vendor'] = $vendor;
        $this->Vendoruser->create();
        if ($this->Vendoruser->save($this->request->data)) {
       }
        $query = [
            'conditions' =>[
                'Vendoruser.vendor' => $vendor
            ],
            'order' => 'Vendoruser.id DESC'
        ];
        $this->Paginator->settings = $query;
        $this->set('members', $this->Paginator->paginate('Vendoruser'));
        }

    }


/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Vendoruser->exists($id)) {
			throw new NotFoundException(__('Invalid vendoruser'));
		}
		$options = array('conditions' => array('Vendoruser.' . $this->Vendoruser->primaryKey => $id));
		$this->set('vendoruser', $this->Vendoruser->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add($vendor_id = null) {
        if(!empty($vendor_id)){
            $vendor = $vendor_id;
        } else {
            $vendor = AuthComponent::user()['id'];
        }

        $query = [
            'conditions' =>[
                'Vendoruser.vendor' => $vendor
            ],
            'order' => 'Vendoruser.id DESC'
        ];
        $this->Paginator->settings = $query;
        $this->set('members', $this->Paginator->paginate('Vendoruser'));

    }

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Vendoruser->exists($id)) {
			throw new NotFoundException(__('Invalid vendoruser'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Vendoruser->save($this->request->data)) {
				$this->Flash->success(__('The vendoruser has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The vendoruser could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Vendoruser.' . $this->Vendoruser->primaryKey => $id));
			$this->request->data = $this->Vendoruser->find('first', $options);
		}
		$users = $this->Vendoruser->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null, $admin=null) {
		$this->Vendoruser->id = $id;
		if (!$this->Vendoruser->exists()) {
			throw new NotFoundException(__('Invalid vendoruser'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Vendoruser->delete()) {
            $this->Session->setFlash(__('The member has been deleted.'));
		} else {
			$this->Session->setFlash(__('The vendoruser could not be deleted. Please, try again.'));
		}
        if(!empty($admin)){
            return $this->redirect(array('action' => 'add', $admin));
        } else {
            return $this->redirect(array('action' => 'add'));

        }
	}
}
