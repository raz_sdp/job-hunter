<?php
App::uses('AppController', 'Controller');
/**
 * Lectures Controller
 *
 * @property Lecture $Lecture
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class LecturesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {

        $keyword = "";
        $conditions = array();
        if(!empty($this->request->data['keyword'])){
            $keyword = $this->request->data['keyword'];
        } elseif(!empty($this->params['named']['keyword'])){
            $keyword = $this->params['named']['keyword'];
        }elseif (!empty($this->params->query['keyword'])) {
            $keyword = $this->params->query['keyword'];
        }
        if (!empty($keyword)){
            $conditions = am($conditions,
                array(
                    'OR'=>
                        array(
                            'Lecture.title LIKE' => '%' . $keyword . '%',
                        ),
                )
            );
        }
        $subject_id = @$this->params->query['subject_id'];
        if(!empty($subject_id)){
            $conditions = am($conditions,[
                'Lecture.subject_id' => $subject_id
            ]);
        } else {
            $subject_id = '';
        }
        $options = array(
            'conditions' => $conditions,
            'order' => 'Lecture.id DESC',
        );


        $this->Paginator->settings = $options;
		$this->Lecture->recursive = 0;
		$this->set('lectures', $this->Paginator->paginate('Lecture'));
        $subjects = $this->Lecture->Subject->find('list');
        $this->set(compact('subjects','keyword','subject_id'));



	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Lecture->exists($id)) {
			throw new NotFoundException(__('Invalid lecture'));
		}
		$options = array('conditions' => array('Lecture.' . $this->Lecture->primaryKey => $id));
		$this->set('lecture', $this->Lecture->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
    public function admin_add()
    {
        if ($this->request->is('post')) {
            //pr($this->request->data);die;

            if (!empty($this->request->data['Lecture']['filename'][0]['name'])) {
                foreach ($this->request->data['Lecture']['filename'] as $item) {

                    $file_name = $this->_upload($item, 'lectures');
                    if (!empty($file_name)) {
                        $this->request->data['Lecture']['filename'] = $file_name;
                        $this->Lecture->create();
                        $this->Lecture->save($this->request->data);
                    }
                }

                $this->Session->setFlash(__('The lecture has been saved.'));
                return $this->redirect(array('action' => 'index'));

            } else {
                $this->Flash->error(__('The lecture could not be saved. Please, try again.'));
            }
        }
        $subjects = $this->Lecture->Subject->find('list');
        $this->set(compact('subjects'));
    }

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_edit($id = null) {
        if (!$this->Lecture->exists($id)) {
            throw new NotFoundException(__('Invalid lecture'));
        }
        if ($this->request->is(array('post', 'put'))) {
            //print_r($this->request->data['Lecture']);die;
            if(!empty($this->request->data['Lecture']['filename']['name'])){
                $file_name = $this->_upload($this->request->data['Lecture']['filename'],'lectures');
                $this->request->data['Lecture']['filename']=$file_name;
            } else{
                unset($this->request->data['Lecture']['filename']);
            }
            $this->Lecture->id = $id;
             //print_r($this->request->data['Lecture']);die;
            if ($this->Lecture->save($this->request->data)) {
                $this->Session->setFlash(__('The lecture has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Flash->error(__('The lecture could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Lecture.' . $this->Lecture->primaryKey => $id));
            $this->request->data = $this->Lecture->find('first', $options);
        }
        $subjects = $this->Lecture->Subject->find('list');
        $this->set(compact('subjects'));
    }

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function admin_delete($id = null) {
        $this->Lecture->id = $id;
        if (!$this->Lecture->exists()) {
            throw new NotFoundException(__('Invalid lecture'));
        }
        $this->request->allowMethod('post', 'delete');


        $this->Lecture->recursive = -1;
        $image = $this->Lecture->findById($id);
        @unlink(WWW_ROOT.'/files/lectures/'.$image['Lecture']['filename']);

        if ($this->Lecture->delete()) {
            $this->Session->setFlash(__('The lecture has been deleted.'));
        } else {
            $this->Flash->error(__('The lecture could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    public function view($sub_id = null){
        $this->Lecture->recursive = -1;
        $query = array(
            'conditions'=>array(
                'Lecture.subject_id' => $sub_id
            )
        );
        $this->Paginator->settings = $query;
        $lec_data = $this->Paginator->paginate('Lecture');

        $subject = $this->Lecture->Subject->findById($sub_id);
        //pr($lec_data);die;
        $this->set(compact('lec_data','subject'));
    }
}
